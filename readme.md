# IADR, built on mdg WordPress Framework

This repository contains:

- **Core Plugin**
A WordPress plugin that provides a wide range of tools, content types, custom fields and other modifications, all of which can be toggled on or off. The core plugin is meant to be used across any number of independent sites.

- **'Custom' Folder within Core Plugin**
Customized for IADR

- **Parent Theme**
A generic WordPress theme designed to work with Core. Includes common PHP functions and page templates that should work for most sites.

- **Child Theme**
Customized for IADR
