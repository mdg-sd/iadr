<?php

// FUNCTIONS
// =========

// DEBUG
// -----

    function mdgcore_debug( $var ) {

    	// Run only in debug mode.
        if ( !defined("WP_DEBUG") || !WP_DEBUG ) return;

        if ( is_string($var) || is_int($var) || is_float($var) ) {
            error_log($var);
        } else {
            error_log( var_export( $var, true ) );
        }

    }

// GET SOURCE
// ----------

	// Used by Custom Post Type helper classes to identify the working post or term.

	if ( !function_exists('mdgcore_get_source_object') ) {
	function mdgcore_get_source_object( $source = null, $is_term = false ) {

		$object = false;

		// Default to global $post (or term object, on archive pages).
		if ( $source && $is_term ) {

			// Accept term object or ID.
			$object = get_term( $source );

		} else if ( $source ) {

			// Accept post object or ID.
			$object = get_post( $source );

		} else if ( get_queried_object() ) {

			$object = get_queried_object();

		} else {

			$post_id = url_to_postid( $_SERVER['REQUEST_URI'] );
			$object  = get_post( $post_id );

		}

		return $object;

	}}

// GET VIRTUAL POST TYPE
// ---------------------

	if ( !function_exists('mdgcore_get_virtual_post_type') ) {
	function mdgcore_get_virtual_post_type ( $source = null ) {

		$source_object = mdgcore_get_source_object( $source );

		if ( empty($source_object) ) { return; }

		// Default to base post type.
		$type = $source_object->post_type;

		// Check whether the post type does not have a "type" taxonomy—such as "post_type", "event_type", etc.
		$subtype_tax = $type.'_type';

		if ( !taxonomy_exists( $subtype_tax ) ) return $type;

		// Get subtype terms on this post, if any.
		$all_terms_on_post = get_the_terms($source_object, $subtype_tax);

		if ( empty($all_terms_on_post) ) return $type;

		// Use term slug as the "virtual" post type.
		$term = $all_terms_on_post[0]; // Presumes only one term.

		$type = $term->slug;

		return $type;

	}}

// DOES POST HAVE SHORTCODE?
// -------------------------

	if ( !function_exists('post_has_shortcode') ) {
	function post_has_shortcode( $shortcode = '', $source = null ) {

		$source = get_post($source);
		if ( !$source || !isset($source->post_content) ) return false;

		$content = $source->post_content;

		return has_shortcode( $content, $shortcode );

	}}

// TRIM SHORTCODE CONTENT
// ----------------------

	if ( !function_exists('mdgcore_trim_shortcode_content') ) {
	function mdgcore_trim_shortcode_content( $content ) {

		$content = preg_replace('/^\s*<\/p>\s*/', '', $content);
		$content = preg_replace('/\s*<p>\s*$/',  '', $content);

		return $content;

	}}

// GET CONTRASTING COLORS
// ----------------------

	if ( !function_exists('mdgcore_get_contrasting_colors') ) {
	function mdgcore_get_contrasting_colors( $hexcolor ) {

		// Trim hash character.
		if ( '#' == substr($hexcolor,0,1) ) {
			$hexcolor = substr($hexcolor, 1);
		}

		$r = hexdec(substr($hexcolor,0,2));
		$g = hexdec(substr($hexcolor,2,2));
		$b = hexdec(substr($hexcolor,4,2));

		$yiq = (($r*299)+($g*587)+($b*114))/1000;
		return ($yiq >= 128) ? '#000' : '#fff';

	}}

// GET FORMATTED DATES
// -------------------

	if ( !function_exists('mdgcore_get_formatted_event_date') ) {
	function mdgcore_get_formatted_event_date( $config = [] ) {

		$start     = isset( $config['start'] )   ? $config['start']   : false;
		$end       = isset( $config['end'] )     ? $config['end']     : false;
		$all_day   = isset( $config['all_day'] ) ? $config['all_day'] : false;
		$no_date   = isset( $config['no_date'] ) ? $config['no_date'] : false;

		if ( !$start ) return;

		// Convert values to PHP DateTime objects.
		$start_date = DateTime::createFromFormat( 'm/d/Y g:i a', $start );

		if ( $end ) {

			$end_date = DateTime::createFromFormat( 'm/d/Y g:i a', $end );

			$same_year  = mdgcore_dates_match( $start_date, $end_date, 'Y' );
			$same_month = mdgcore_dates_match( $start_date, $end_date, 'Y-m' );
			$same_day   = mdgcore_dates_match( $start_date, $end_date, 'Y-m-d' );
			$same_apm   = mdgcore_dates_match( $start_date, $end_date, 'Y-m-d a' );
			$same_time  = mdgcore_dates_match( $start_date, $end_date, 'Y-m-d H:i' );

		}

		// Determine how to format start & end.
		if ( !$end || $same_time ) {

			if ( $all_day ) {
				$start_format = 'F j, Y';
			} elseif ( $no_date ) {
				$start_format = 'g:i a';
			} else {
				$start_format = 'F j, Y g:i a';
			}

		} else {

			if ( $all_day ) {

				if ( $same_day ) {
					$start_format = 'F j, Y';
					$end_format   = false;
				} elseif ( $same_month ) {
					$start_format = 'F j';
					$end_format   = 'j, Y';
				} elseif ( $same_year ) {
					$start_format = 'F j';
					$end_format   = 'F j, Y';
				} else {
					$start_format = 'F j, Y';
					$end_format   = 'F j, Y';
				}

			} elseif ( $no_date ) {

				if ( $same_apm ) {
					$start_format = 'g:i';
					$end_format   = 'g:i a';
				} else {
					$start_format = 'g:i';
					$end_format   = 'g:i a';
				}

			} else {

				if ( $same_apm ) {
					$start_format = 'F j, Y g:i';
					$end_format   = 'g:i a';
				} elseif ( $same_day ) {
					$start_format = 'F j, Y g:i a';
					$end_format   = 'g:i a';
				} else {
					$start_format = 'F j, Y g:i a';
					$end_format   = 'F j, Y g:i a';
				}

			}

		}

		// FINISH
		$output = '';

		if ( $end && $end_format ) {
			$output = sprintf( '%s&ndash;%s', $start_date->format( $start_format ), $end_date->format( $end_format ) );
		} else {
			$output = $start_date->format( $start_format );
		}

		return $output;

	}}

	if ( !function_exists('mdgcore_dates_match') ) {
	function mdgcore_dates_match( $start_date, $end_date, $format ) {
		return ( $start_date->format($format) == $end_date->format($format) );
	}}

// ALLOW LINKS IN EXCERPT
// ----------------------

	function iadr_html_excerpt( $text ) { // Fakes an excerpt if needed
		global $post;
		if ( '' == $text ) {
			$text = get_the_content('');
			$text = apply_filters('the_content', $text);
			$text = str_replace('\]\]\>', ']]&gt;', $text);
			/*just add all the tags you want to appear in the excerpt --
			be sure there are no white spaces in the string of allowed tags */
			$text = strip_tags($text,'<a><em><strong>');
			/* you can also change the length of the excerpt here, if you want */
			$excerpt_length = 55;
			$words = explode(' ', $text, $excerpt_length + 1);
			if (count($words)> $excerpt_length) {
				array_pop($words);
				array_push($words, '[...]');
				$text = implode(' ', $words);
			}
		}
		return $text;
	}
	/* remove the default filter */
	remove_filter('get_the_excerpt', 'wp_trim_excerpt');

	/* now, add your own filter */
	add_filter('get_the_excerpt', 'iadr_html_excerpt');