<?php

// BLOCK: IMAGE SWITCHER
// ======================

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_img_switcher_setup', 0 );

	if ( !function_exists('mdgcore_block_img_switcher_setup') ) {
	function mdgcore_block_img_switcher_setup() {

		// RENDERER
		mdgcore_register_block('img_switcher', 'mdgcore_render_block_img_switcher');
		wp_enqueue_script ( 'greensock', get_stylesheet_directory_uri() . '/js/lib/greensock/TweenMax.min.js', [], false );

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_img_switcher') ) {
	function mdgcore_render_block_img_switcher ( $block ) {

		if ( !isset($block['images']) || empty($block['images']) ) return;

		$images = $block['images'];

		?>

		<section class="entry-block-img-switcher d-flex flex-column justify-content-center rounded px-2">

			<?php if ( !empty( $block['link'] ) ) { ?>
			<a class="heading-styled" href="<?=$block['link']['url']?>" target="<?=$block['link']['target']?>"><?=$block['title']?></a>
			<?php } else { ?>
			<div class="heading-styled"><?=$block['title']?></div>
			<?php } ?>
			<ul class="switcher-nav px-0 mx-2">
				<?php foreach( $images as $img ): ?>
				<li>
					<button class="btn btn-light px-2 btn-sm mb-1 img-switcher-btn" role="button"><?=$img['image']['title']; ?></button>
				</li>
				<?php endforeach; ?>
			</ul>
			<ul id="entry-block-switcher-content" class="img-list">
				<?php foreach( $images as $img ): ?>
					<li class="img-item" id="<?=str_replace('#','',$img['image']['title'])?>">
						<?php if ( $img['image_size'] === true ) { ?>
							<img src="<?=$img['image']['sizes']['switcher']?>" alt="<?=$img['image']['title']?>" />
						<?php } else {  ?>
							<img src="<?=$img['image']['url']?>" alt="<?=$img['image']['title']?>" />
						<?php } ?>
					</li>
				<?php endforeach; ?>
			</ul>

			<script type="text/javascript">

				jQuery(document).ready(function(){

					var zIndex 		= 0,
						lastImage	= null,
						$maxImg		= '',
						count 		= $( '.img-switcher-btn' ).length;

					$( '.img-switcher-btn' ).each( function(i){

						var $btn			= $( this ),
							$id				= '#' + $btn.text().replace(/[^A-Z0-9]/ig, ""),
							$li				= $( '.img-list' ).find( $id ),
							$img 			= $li.find( 'img' ),
							$maxHeight 		= 400;

						$img.one('load', function(){
							// grab img source
							var tmpImg 		= new Image();
								tmpImg.src 	= $img.attr( 'src' ),
								$width 		= tmpImg.width,
								$height 	= tmpImg.height;

							// apply to image
							$img.css( { 'max-height': $height, 'max-width': $width } );

							// make sure to update the slider height to accomodate
							if ( $height > $maxHeight ) {
								$maxHeight 	= $height;
								$maxImg 	= $img.attr( 'src' );
							}

							if ( i + 1 === count ) {

								// make sure to update the slider height to accomodate
								if ( $maxImg != '' ) {
									$( '.img-list' ).append( '<li class="sizer"><img src="' + $maxImg + '"/>' );
								}
							}
							}).each(function() {
								if(this.complete) {
									$(this).load(); // For jQuery < 3.0
									// $(this).trigger('load'); // For jQuery >= 3.0
								}
							}
						);

						$li.on( 'hover', function( e ){
							TweenMax.killAll( true );
						});

						$btn.mouseenter(function( e ){
							TweenMax.killAll( true );
							nextImage( $li );
						});

						if ( $id === '<?=$images[0]['image']['title']; ?>' ){
							nextImage( $li );
						}

					});

					function nextImage( $img ) {

						if ( $img != lastImage ) {
							zIndex++;
							$img.css( 'z-index', zIndex );
							TweenMax.fromTo( $img, .75, { opacity: 0 }, { opacity: 1, ease: Power3.easeOut });
							if ( lastImage !== null ) TweenMax.to( lastImage, .5, { opacity: 0, ease: Power3.easeOut });
							lastImage = $img;
						}
					}

				});

			</script>

		</section>

		<?php

	}}