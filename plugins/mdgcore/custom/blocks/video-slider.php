<?php

// BLOCK: VIDEO SLIDER
// ====================

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_video_slider_setup', 0 );

	if ( !function_exists('mdgcore_block_video_slider_setup') ) {
	function mdgcore_block_video_slider_setup() {

		// RENDERER
		mdgcore_register_block('video_slider', 'mdgcore_render_block_video_slider');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_video_slider') ) {
	function mdgcore_render_block_video_slider ( $block ) {

		if ( !isset($block['slides']) || empty($block['slides']) ) return;

		$settings 	= array(
			'displayTitle'		=> $block['settings']['display_title'],
			'infinite'			=> json_encode( $block['settings']['infinite'] ),
			'slidesToShow'		=> +$block['settings']['slidesToShow'],
			'slidesToScroll'	=> +$block['settings']['slidesToScroll'],
			'arrows'			=> json_encode( $block['settings']['arrows'] ),
			'autoplay'			=> json_encode( $block['settings']['autoplay'] ),
			'isResponsive'		=> $block['settings']['responsive']
		);

		if ( $settings['isResponsive'] ) $settings['breakpoints'] = $block['settings']['breakpoints'];
		if ( json_decode( $settings['autoplay'] ) ) $settings['autoplaySpeed'] = +$block['settings']['autoplaySpeed'] * 1000;

		$slides 	= [];

		foreach ( $block['slides'] as $data ) {

			$slide = [
				'title'				=> $data['title'],
				'link'				=> $data['link'],
				'video'				=> $data['video']
			];

			$slides[] = $slide;

		}

		$b = uniqid();

		?>

		<section class="entry-block-slider video-slider pb-3">

			<?php if ( !empty( $block['link'] ) ) {
					if ( !empty( $block['link'] ) ) { ?>
			<a class="heading-styled" href="<?=$block['link']['url']?>" target="<?=$block['link']['target']?>"><?=$block['title']?></a>
			<?php } else { ?>
			<div class="heading-styled"><?=$block['title']?></div>
			<?php } } ?>

			<ul id="entry-block-slider-content-<?=$b?>" class="slider-list">
				<?php foreach( $slides as $slide ):
					// get iframe HTML
					$iframe = $slide['video'];

					// use preg_match to find iframe src
					preg_match( '/src="(.+?)"/', $iframe, $matches );
					$src = $matches[1];

					$params = array(
						'controls'	=> 0,
						'hd'		=> 1,
						'autohide'	=> 1
					);

					$new_src = add_query_arg( $params, $src );

					$iframe = str_replace( $src, $new_src, $iframe );
					$attributes = 'frameborder="0"';

					$iframe = str_replace( ' width="640" height="360"', '', $iframe );
					$iframe = str_replace( '></iframe>', ' ' . $attributes . '></iframe>', $iframe ); ?>
					<li class="slide-item">
						<div class="video-container">
							<div>
								<?=$iframe; ?>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php if ( $settings['arrows'] ) { ?>
			<button class="btn btn-secondary btn-sm video-prev px-3 mr-3 mb-2" aria-label="Previous Video" type="button">Previous Video</button>
			<button class="btn btn-secondary btn-sm video-next px-3 ml-3 mb-2" aria-label="Next Video" type="button">Next Video</button>
			<?php } ?>

			<script type="text/javascript">

				jQuery( document ).ready( function(){

					var videoCarousel = jQuery('#entry-block-slider-content-<?=$b?>');

					videoCarousel.slick ( {
						infinite: 					<?=$settings['infinite']?>,
						slidesToShow: 				<?=$settings['slidesToShow']?>,
						slidesToScroll: 			<?=$settings['slidesToScroll']?>,
					<?php if ( $settings['arrows'] ) { ?>
						prevArrow: 					'.video-prev',
						nextArrow: 					'.video-next',
					<?php } ?>
					<?php if ( json_decode( $settings['autoplay'] ) ) { ?>
						autoplay: 					<?=$settings['autoplay']?>,
						autoplaySpeed: 				<?=$settings['autoplaySpeed']?>,
					<?php } ?>
					<?php if ( $settings['isResponsive'] ) { ?>
						responsive: [
							<?php foreach( $settings['breakpoints'] as $breakpoint ): ?>
							{	breakpoint: 		<?=+$breakpoint['breakpoint']?>,
								settings: {
									slidesToShow: 	<?=+$breakpoint['slidesToShow']?>,
									slidesToScroll: <?=+$breakpoint['slidesToScroll']?>,
								},
							},
							<?php endforeach; ?>
						],<?php } ?>
					});

					// Find all YouTube videos
					var $allVideos = $( '.entry-block-slider' ),

					// The element that is fluid width
					$fluidEl = $( '.main-container' );

					// Figure out and save aspect ratio for each video
					$allVideos.each( function() {

						$( this )
						.data( 'aspectRatio', this.height / this.width )

						// and remove the hard coded width/height
						.removeAttr( 'height' )
						.removeAttr( 'width' );

					});

					// When the window is resized
					$( window ).resize( function() {

					var newWidth = $fluidEl.width() - 88;

					// Resize all videos according to their own aspect ratio
					$allVideos.each( function() {

						var $el = $( this );
						$el
							.width( newWidth )
							.height( newWidth * $el.data('aspectRatio'));

						});

					// Kick off one resize to fix all videos on page load
					}).resize();

				});

			</script>

		</section>

		<?php

	}}