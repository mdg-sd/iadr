<?php

// BLOCK: HEADING STYLED
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_heading_styled_setup', 0 );

	if ( !function_exists('mdgcore_block_heading_styled_setup') ) {
	function mdgcore_block_heading_styled_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('heading-styled', 'mdgcore_render_block_heading_styled');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_heading_styled') ) {
	function mdgcore_render_block_heading_styled ( $block ) {

		$id = sanitize_title($block['normal']);

		?>
			<div class="heading-styled"><?=$block['normal']?> <strong><?=$block['bold']?></strong></div>

		<?php

	}}
