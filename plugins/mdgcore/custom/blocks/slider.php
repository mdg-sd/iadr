<?php

// BLOCK: SLIDER
// ====================

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_slider_setup', 0 );

	if ( !function_exists('mdgcore_block_slider_setup') ) {
	function mdgcore_block_slider_setup() {

		// RENDERER
		mdgcore_register_block('slider', 'mdgcore_render_block_slider');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_slider') ) {
	function mdgcore_render_block_slider ( $block ) {

		if ( !isset($block['slides']) || empty($block['slides']) ) return;

		$settings 	= array(
			'infinite'			=> json_encode( $block['settings']['infinite'] ),
			'slidesToShow'		=> +$block['settings']['slidesToShow'],
			'slidesToScroll'	=> +$block['settings']['slidesToScroll'],
			'arrows'			=> json_encode( $block['settings']['arrows'] ),
			'autoplay'			=> json_encode( $block['settings']['autoplay'] ),
			'isResponsive'		=> $block['settings']['responsive']
		);

		if ( $settings['isResponsive'] ) $settings['breakpoints'] = $block['settings']['breakpoints'];
		if ( $settings['autoplay'] ) $settings['autoplaySpeed'] = +$block['settings']['autoplaySpeed'] * 1000;

		$slides 	= [];

		foreach ( $block['slides'] as $data ) {

			$image_main_url  = $data['image']['sizes']['slider'];

			$slide = [
				'alt'				=> $data['alt'],
				'link'				=> $data['link'],
				'image_main_url'	=> $image_main_url
			];

			$slides[] = $slide;

		}

		$b = uniqid();

		?>

		<section class="entry-block-slider">

			<?php if ( !empty( $block['link'] ) ) { ?>
			<a class="heading-styled" href="<?=$block['link']['url']?>" target="<?=$block['link']['target']?>"><?=$block['title']?></a>
			<?php } else { ?>
			<div class="heading-styled"><?=$block['title']?></div>
			<?php } ?>

			<ul id="entry-block-slider-content-<?=$b?>" class="slider-list">
				<?php foreach( $slides as $slide ): ?>
					<li class="slide-item">
						<?php if ( $slide['link'] ) { ?><a title="<?=$slide['link']['title']; ?>" href="<?=$slide['link']['url']; ?>" target="<?=$slide['link']['target']; ?>"><?php } ?>
							<img src="<?=$slide['image_main_url']; ?>" alt="<?=$slide['alt']; ?>" />
						<?php if ( $slide['link'] ) { ?></a><?php } ?>
					</li>
				<?php endforeach; ?>
			</ul>

			<script type="text/javascript">

				jQuery(document).ready(function(){

					var carousel = jQuery('#entry-block-slider-content-<?=$b?>');

					carousel.slick ( {

						infinite: 					<?=$settings['infinite']?>,
						slidesToShow: 				<?=$settings['slidesToShow']?>,
						slidesToScroll: 			<?=$settings['slidesToScroll']?>,
						arrows: 					<?=$settings['arrows']?>,
					<?php if ( json_decode( $settings['autoplay'] ) ) { ?>
						autoplay: 					<?=$settings['autoplay']?>,
						autoplaySpeed: 				<?=$settings['autoplaySpeed']?>,
					<?php } ?>
					<?php if ( $settings['isResponsive'] ) { ?>
						responsive: [
							<?php foreach( $settings['breakpoints'] as $breakpoint ): ?>
							{	breakpoint: 		<?=+$breakpoint['breakpoint']?>,
								settings: {
									slidesToShow: 	<?=+$breakpoint['slidesToShow']?>,
									slidesToScroll: <?=+$breakpoint['slidesToScroll']?>,
								},
							},
							<?php endforeach; ?>
						],<?php } ?>
					});

				});

			</script>

		</section>

		<?php

	}}