<?php

// BLOCK: COLUMNS
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_columns_setup', 0 );

	if ( !function_exists('mdgcore_block_columns_setup') ) {
	function mdgcore_block_columns_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('columns', 'mdgcore_render_block_columns');

		// SHORTCODE
		add_shortcode('columns', 'mdgcore_shortcode_columns');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_columns') ) {
	function mdgcore_render_block_columns ( $block ) {

		if ( !isset($block['columns']) || empty($block['columns']) ) return; ?>

		<section class="entry-columns">
			<div class="entry-block-inner">

				<?php foreach ($block['columns'] as $column) { ?>
				<div class="entry-column">
					<?php
						if ( !empty( $column['link'] ) ) { ?>
						<a href="<?=$column['link']['url'];?>" target="<?=$column['link']['target'];?>" title="<?=$column['link']['title'];?>">
					<?php }
						if ( isset( $column['featured-image']) ) { ?>
						<?=wp_get_attachment_image( $column['featured-image'], 'home-column', false, [ 'class' => 'blue-border' ] ); ?>
					<?php }
						if ( isset( $column['title']) ) { ?>
						<h4 class="text-center"><?=$column['title']?></h4>
					<?php }
						if ( !empty( $column['link'] ) ) { ?>
						</a>
					<?php } ?>

					<?=$column['content']?>

				</div>
				<?php } ?>

			</div>
		</section>

		<?php

	}}

// SHORTCODE
// ---------

	if ( !function_exists('mdgcore_shortcode_columns') ) {
	function mdgcore_shortcode_columns($atts, $content = null) {

		if ( !$content ) { return; }

		// Parse content for columns.
		preg_match_all('/\[column\](.*)\[\/column\]/sU', $content, $columns);

		if( empty($columns) || empty($columns[1]) ) { return; }

		// Set up an ad-hoc "block" to render.
		$block = [
			'columns' => []
		];

		foreach ($columns[1] as $column_content) {
			$block['columns'][] = [
				'content' => mdgcore_trim_shortcode_content( $column_content ),
			];
		}

		ob_start();

		mdgcore_render_columns( $block );

		$output = ob_get_contents();

		ob_end_clean();

		return $output;

	}}
