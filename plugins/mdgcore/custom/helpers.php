<?php



// CUSTOM NAV WALKER - TITLES / ICONS
// converts nav items to font-awesome icons with fa classes added

	class Class_Filters extends Walker_Nav_Menu {

		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

			$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );
			$class_names = $value = '';

			// Passed classes
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;

			// Remove icon class from list item
			if ( $icon_class 	= preg_grep( '/fa-/', $classes ) ) {
				 $classes 		= array_diff( $classes, $icon_class );
			} else {
				if ( $item->menu_item_parent === '0' && $output !== '' ) {
					$output .= "<li class=\"divider\">|</li>\n";
				}
			}

			/* Add active class */
			if ( in_array( 'current-menu-item', $classes ) ) {
				$classes[] = 'active';
				unset( $classes['current-menu-item'] );
			}

			/* Check for children */
			$children = get_posts( array(
				'post_type' 	=> 'nav_menu_item',
				'nopaging' 		=> true,
				'numberposts' 	=> 1,
				'meta_key' 		=> '_menu_item_menu_item_parent',
				'meta_value' 	=> $item->ID
			));

			if ( !empty( $children ) ) {
				$classes[] = 'has-sub';
			}

			$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

			// Build HTML
			$class_names 	= $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
			$id 			= apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id 			= $id ? ' id="' . esc_attr( $id ) . '"' : '';
			$output 		.= $indent . '<li' . $id . $value . $class_names .'>';

			// If 'menuCategory' exists in classes, don't HTML anchor tag.
			if( in_array( 'menu-category', $classes ) ) {

				$item_output = apply_filters( 'the_title', $item->title, $item->ID );

			} else {

				// Link attributes
				$atts = array();
				$atts['title']	= !empty( $item->attr_title )	? $item->attr_title : '';
				$atts['target'] = !empty( $item->target )		? $item->target		: '';
				$atts['rel']	= !empty( $item->xfn )			? $item->xfn		: '';

				if ( in_array( 'has-sub', $classes ) ) { $atts['class'] = 'top'; }

				$atts['href']   = !empty( $item->url )	? $item->url		: '';

				$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
				$attributes = '';
				foreach ( $atts as $attr => $value ) {
					if ( !empty( $value ) && $value !== '#' ) {
						$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
						$attributes .= ' ' . $attr . '="' . $value . '"';
					}
				}
				$item_output = $args->before;
				$item_output .= '<a'. $attributes .'>';

				// Add an icon if assigned
				if ( $icon_class ){ $item_output .= '<i class="fab ' . current( $icon_class ) . '" aria-hidden="true" title="' . apply_filters( 'the_title', $item->title, $item->ID ).'"><span class="sr-only">'; }
				$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
				if ( $icon_class ){ $item_output .= '</span></i>'; }
				$item_output .= '</a>';
				$item_output .= $args->after;
			}

			// build html
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}


// DESAT & SET AS BKG

	function desat_bkg( $url ) { // retiring this for now as this won't need to be dynamic

		$path 		= str_replace( home_url(), '', pathinfo( $url )[ 'dirname' ] ) . '/';
		$basename 	= basename( basename( $url ), '.jpg' );

		// BUFFER OUTPUT
		ob_start();

		$image 		= imagecreatefromstring( wp_remote_get( $url )[ 'body' ] );

		imagefilter( $image, IMG_FILTER_GRAYSCALE );
		imagefilter( $image, IMG_FILTER_BRIGHTNESS, 65 );

		imagejpeg( $image, $path . $basename );

		// GRAB/CLEAR/END BUFFER
		$buffer 	= ob_get_clean();
		$bkg_style 	= ' style="background-image:url(\'data:image/png;base64,'. base64_encode( $buffer ).'\')"';

		return $bkg_style;
	}


	/**
	 * Save year taxonomy acf fields in terms as post meta for query purposes on publish
	 */
	function timeline_query_acf_save_post_meta( $post_id ) {

		if ( !has_term( '', 'timeline_year', $post_id ) ) return; // only if a year's been selected

		$field 		= get_field( 'year', $post_id );
		$year 		= $field->name;
		$meta_tag 	= $field->taxonomy . '_sort';
		$decades 	= get_terms( [ 'taxonomy' => 'timeline_decade', 'hide_empty' => false ] );

		// add meta for filtering
		if ( $field ) update_post_meta( $post_id, $meta_tag, floatval( $year ) );

		// add decade to match year chosen
		foreach ( $decades as $decade ) {
			if ( substr( $year, 0, 3 ) === substr( $decade->name, 0, 3 ) ) {
				wp_set_object_terms( $post_id, $decade->slug, $decade->taxonomy, false );
			}
		}
	}
	add_filter( 'acf/save_post', 'timeline_query_acf_save_post_meta', 20, 3 );


	/**
	 * rewrite decade field json as text instead of taxonomy select, with decade as value
	 */
	function return_acf_tax_as_text( $field ) {

		global 		$post;
		$tax_id 	= get_field( $field['_name'], $post->ID );

		if ( !$tax_id ) return $field; // if nothing is set, exit

		$term 		= get_term( $tax_id, $field['taxonomy'] );

		$new_field = [
			'ID' 				=> $field['ID'],
			'key' 				=> $field['key'],
			'label' 			=> $field['label'],
			'name' 				=> $field['name'],
			'prefix' 			=> 'acf',
			'type' 				=> 'text',
			'value' 			=> $term->name,
			'menu_order' 		=> $field['menu_order'],
			'instructions' 		=> $field['instructions'],
			'required' 			=> $field['required'],
			'id' 				=> $field['id'],
			'class' 			=> $field['class'],
			'conditional_logic' => $field['conditional_logic'],
			'parent' 			=> $field['parent'],
			'wrapper' 			=>
				[
					'width' => '',
					'class' => '',
					'id' 	=> '',
				],
			'_name' 			=> $field['_name'],
			'_prepare' 			=> 1,
			'_valid' 			=> 1,
			'default_value' 	=> '',
			'placeholder' 		=> '',
			'prepend' 			=> '',
			'append' 			=> '',
			'maxlength' 		=> '',
			'readonly'			=> 1,
			'disabled'			=> 1
		];
		return $new_field;

	}
	add_filter('acf/prepare_field/name=decade', 'return_acf_tax_as_text');
