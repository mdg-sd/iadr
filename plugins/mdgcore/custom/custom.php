<?php

# Add site-specific customizations here.

include( MDGCORE_PATH . 'custom/helpers.php');

include( MDGCORE_PATH . 'custom/content/timeline-post.php');

include( MDGCORE_PATH . 'custom/content/past-president.php');

include( MDGCORE_PATH . 'custom/blocks/heading-styled.php');

include( MDGCORE_PATH . 'custom/blocks/columns.php');

include( MDGCORE_PATH . 'custom/blocks/slider.php');

include( MDGCORE_PATH . 'custom/blocks/video-slider.php');

include( MDGCORE_PATH . 'custom/blocks/image-switcher.php');