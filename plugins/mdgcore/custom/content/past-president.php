<?php

// PAST PRESIDENTS
// ===============

// Custom post type for past president profile
// Duplicated from core mdg plugin profiles functionality, with adjusted naming

// SETUP
// -----

	add_action( 'init', 'mdgcore_president_setup', 0 );

	if ( !function_exists('mdgcore_president_setup') ) {

	function mdgcore_president_setup() {

		add_action( 'init', 'mdgcore_regcpt_president' );
		add_action( 'init', 'mdgcore_regctx_president_category' );

	}}

// CUSTOM POST TYPE
// ----------------

	if ( !function_exists('mdgcore_regcpt_president') ) {

	function mdgcore_regcpt_president() {

		register_post_type( 'profile', [
			'supports'            => [ 'title', 'editor', 'thumbnail' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-groups',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Past Presidents',
				'singular_name'       => 'Past President',
				'menu_name'           => 'Past Presidents',
				'parent_item_colon'   => 'Parent President:',
				'all_items'           => 'All Presidents',
				'view_item'           => 'View President',
				'add_new_item'        => 'Add New President',
				'add_new'             => 'New President',
				'edit_item'           => 'Edit President',
				'update_item'         => 'Update President',
				'search_items'        => 'Search Presidents',
				'not_found'           => 'No Presidents Found',
				'not_found_in_trash'  => 'No Presidents Found in Trash',
			],
		]);

	}}

// CUSTOM TAXONOMY: PROFILE CATEGORY
// ---------------------------------

	if ( !function_exists('mdgcore_regctx_president_category') ) {

	function mdgcore_regctx_president_category()  {

		register_taxonomy( 'profile_category', ['profile'], [
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => ['slug' => 'profile-category'],
			'labels'                     => [
				'name'                       => 'President Categories',
				'singular_name'              => 'President Category',
				'menu_name'                  => 'Categories',
				'all_items'                  => 'All Categories',
				'parent_item'                => 'Parent Category',
				'parent_item_colon'          => 'Parent Category:',
				'new_item_name'              => 'New Category',
				'add_new_item'               => 'Add New Category',
				'edit_item'                  => 'Edit Category',
				'update_item'                => 'Update Category',
				'separate_items_with_commas' => 'Separate Categories with Commas',
				'search_items'               => 'Search Categories',
				'add_or_remove_items'        => 'Add or Remove Categories',
				'choose_from_most_used'      => 'Choose from the most used Categories',
			],
		]);

	}}
