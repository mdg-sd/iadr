<?php

// ADS
// ===

// Custom post type for advertisements.

// SETUP
// -----

	add_action( 'init', 'mdgsite_timeline_setup', 0 );

	function mdgsite_timeline_setup() {

		add_action( 'init', 'mdgsite_regcpt_timeline' );
		add_action( 'init', 'mdgsite_regctx_timeline_year' ); 	// tag
		add_action( 'init', 'mdgsite_regctx_timeline_decade' ); 	// category

		add_action( 'admin_head', 'mdgsite_admin_timeline_styles' );

	}

// CUSTOM POST TYPE
// ----------------

	function mdgsite_regcpt_timeline() {

		register_post_type( 'timeline', [
			'supports'            => [ 'title', 'editor', 'thumbnail', 'custom-fields', 'revisions', 'post-formats', 'excerpt' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-calendar-plus',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Timeline Posts',
				'singular_name'       => 'Timeline Post',
				'menu_name'           => 'Timeline Posts',
				'all_items'           => 'All Timeline Posts',
				'view_item'           => 'View Timeline Post',
				'add_new_item'        => 'Add New Timeline Post',
				'add_new'             => 'New Timeline Post',
				'edit_item'           => 'Edit Timeline Post',
				'update_item'         => 'Update Timeline Post',
				'search_items'        => 'Search Timeline Posts',
				'not_found'           => 'No Timeline Posts Found',
				'not_found_in_trash'  => 'No Timeline Posts Found in Trash',
			],
		]);

	}



	// CUSTOM TAXONOMY: TIMELINE YEAR (tag)
	// ---------------------------

	function mdgsite_regctx_timeline_year() {

		register_taxonomy( 'timeline_year', [ 'timeline' ], [
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_quick_edit'         => false,
			'meta_box_cb'                => false,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => [ 'slug' => 'timeline-year' ],
			'labels'                     => [
				'name'                       => 'Timeline Years',
				'singular_name'              => 'Timeline Year',
				'menu_name'                  => 'Years',
				'all_items'                  => 'All Years',
				'parent_item'                => 'Parent Year',
				'parent_item_colon'          => 'Parent Year:',
				'new_item_name'              => 'New Year',
				'add_new_item'               => 'Add New Year',
				'edit_item'                  => 'Edit Year',
				'update_item'                => 'Update Year',
				'separate_items_with_commas' => 'Separate Years with Commas',
				'search_items'               => 'Search Years',
				'add_or_remove_items'        => 'Add or Remove Years',
				'choose_from_most_used'      => 'Choose from the most used Years',
			],
		]);
	}

	// CUSTOM TAXONOMY: TIMELINE DECADE (category)
	// -------------------------------

	function mdgsite_regctx_timeline_decade() {

		register_taxonomy( 'timeline_decade', [ 'timeline' ], [
			'hierarchical'               => false,
			'public'                     => true,
			'show_ui'                    => true,
			'show_in_quick_edit'         => false,
			'meta_box_cb'                => false,
			'show_admin_column'          => false,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => [ 'slug' => 'timeline-decade' ],
			'labels'                     => [
				'name'                       => 'Timeline Decades',
				'singular_name'              => 'Timeline Decade',
				'menu_name'                  => 'Decades',
				'all_items'                  => 'All Decades',
				'parent_item'                => 'Parent Decade',
				'parent_item_colon'          => 'Parent Decade:',
				'new_item_name'              => 'New Decade',
				'add_new_item'               => 'Add New Decade',
				'edit_item'                  => 'Edit Decade',
				'update_item'                => 'Update Decade',
				'separate_items_with_commas' => 'Separate Decades with Commas',
				'search_items'               => 'Search Decades',
				'add_or_remove_items'        => 'Add or Remove Decades',
				'choose_from_most_used'      => 'Choose from the most used Decades',
			],
		]);
	}

	// CUSTOM FONT ICON
	// ----------------

	function mdgsite_admin_timeline_styles() {

		//wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', '', '5.5.0', 'all' );
		echo '<link href="//use.fontawesome.com/releases/v5.5.0/css/all.css"  rel="stylesheet">
		<style>
			.dashicons-before:before { font-family: dashicons, "Font Awesome 5 Free"; }
			.dashicons-calendar-plus:before { padding-top:4px !important; content: "\f271"; }
			body.taxonomy-timeline_decade form#edittag tr.term-description-wrap { display: none; }
		</style>';

	}

	// AJAX LOAD TIMELINE POSTS
	// ------------------------

	add_action( 'rest_api_init', function () {
		register_rest_route( 'namespace/v1', '/events', [
			'methods'  => 'POST',
			'callback' => 'timeline_get_events',
		]);
	});

	function timeline_get_events( $request ) {

		$decade = isset($request['decade']) ? $request['decade'] : null;

		$get_years = new WP_Query( [
			'posts_per_page' => -1,
			'post_type'	=> 'timeline',
			'order'		=> 'ASC',
			'orderby'	=> 'meta_value_num',
			'meta_type'	=> 'NUMERIC',
			'meta_key' 	=> 'timeline_year_sort',
			'tax_query'	=> [[
				'taxonomy'	=> 'timeline_decade',
				'field'		=> 'slug',
				'terms'		=> $decade,
				'operator'	=> 'IN',
				'include_children'	=> false
			]]
		] );

		$alternate 	= ($decade === '2000') ? '':' alternate'; // attempt to troubleshoot 2003 showing before 2001 in the list
		$html 		= '';

		while ( $get_years->have_posts() ){
			$post 				= $get_years->the_post();
			$year				= get_field( 'year' );
			$read_more			= get_field( 'read_more' );
			$alternate 			= empty( $alternate ) ? ' alternate' : '';
			$expand_type 		= empty( $alternate ) ? 'expand-left' : 'expand-right';
			$zoom_type 			= empty( $alternate ) ? 'zoom-left' : 'zoom-right';
			$has_image			= has_post_thumbnail();
			$image_tag 			= '';
			$pad_top 			= '';
			$excerpt_pad 		= '';
			$read_more_link		= '<br/>';
			$more_content		= '';
			$do_trigger			= '';
			$yearID				= get_the_ID();
			$the_content 		= get_the_content();
			$the_excerpt 		= get_the_excerpt();
			$the_title 			= get_display_title();
			// superscript ordinals
			$the_title 			= preg_replace( '/(\d)+(st|nd|rd|th)([^\w\d]|$)/', '$1<sup>$2</sup>$3', $the_title );

			if ( !empty( $the_excerpt ) ) $excerpt_pad = ' pt-2 pb-3';

			if ( $read_more === true || $read_more === NULL ) {
				$read_more_link = '<p class="trigger font-weight-bold">Read More <i class="fas fa-long-arrow-alt-right"></i></p>';
				$more_content 	= '<div class="year-etc">

								<div class="etc-content">' .

									$the_content .

								'</div>

							</div>';
				$do_trigger		= ' trigger';
			}

			if ( $has_image ) {
				$imgdata 		= wp_get_attachment_image_src( get_post_thumbnail_id(), 'max-timeline' );
				$imgurl 		= $imgdata[0];
				$imgwidth 		= $imgdata[1];
				$imgheight 		= $imgdata[2];
				$attribution	= get_field( 'attribution' ) ?: '';
				if ( !empty( $attribution ) ){
					$attribution = strip_tags(	$attribution, '<p><div><a>	' );
					$attribution = str_replace( '<div>', '<i class="fal fa-info-square"></i><div class="attribution">PHOTO CREDIT: ', $attribution );
					$attribution = str_replace( '<p>', '<i class="fal fa-info-square"></i><div class="attribution">PHOTO CREDIT: ', $attribution );
					$attribution = str_replace( '</p>', '</div>', $attribution );
				}
				$image_tag 		= '<div class="entry-image' . $do_trigger . '"><img src="' . $imgurl . '" ondragstart="return false;" data-img-height="' . $imgheight . '">' . $attribution . '</div>';
			} else {
				$pad_top 		= 'pt-5';
			}

			$html .= '<li id="year-' . $yearID . '" class="timeline-year ' . $alternate . '">



				<div class="item-link">
					<div class="link-line" data-aos="' . $expand_type . '" data-aos-anchor="#year-panel-' . $yearID . '" data-aos-anchor-placement="top-bottom" data-aos-delay="350"></div>
					<div class="link-dot" data-aos="link-dot" data-aos-anchor="#year-panel-' . $yearID . '" data-aos-anchor-placement="top-bottom"></div>
				</div>

				<div class="year-panel" data-aos="panel-slide" data-aos-delay="700" data-aos-duration="700" data-aos-anchor="#year-panel-' . $yearID . '" data-aos-anchor-placement="top-bottom" data-aos-id="panel-animate-' . $yearID . '">' .

					$image_tag .

					'<div class="entry-content-container">

						<h1 class="entry-title ' . $pad_top . '">' . $the_title . '</h1>

						<section class="entry-content">

							<div class="entry-content-inner' . $excerpt_pad . '">' .

								$the_excerpt .

								$read_more_link .

							'</div>

						</section>

					</div>' .

					$more_content .

					'<i class="fas fa-times trigger etc-close"></i>

				</div>

				<div class="year-badge" data-aos="' . $zoom_type . '" data-aos-anchor="#year-panel-' . $yearID . '" data-aos-anchor-placement="top-bottom" data-aos-delay="600">' . $year->name . '</div>

			</li>';

			wp_reset_postdata();
		}

		$output = [
			'html' => $html
		];

		return $output;

	}

	// ADMIN SORT BY TIMELINE YEAR TAX
	// -------------------------------
	// Column Header
	add_filter( 'manage_timeline_posts_columns', 'set_custom_edit_timeline_columns' );

	function set_custom_edit_timeline_columns( $columns ) {

		$columns[ 'timeline-years' ]	= 'Year';
		$columns[ 'modified-last' ]		= 'Last Modified';
		return $columns;

	}

	// Column Content
	add_action( 'manage_timeline_posts_custom_column' , 'custom_timeline_column', 10, 2 );

	function custom_timeline_column( $column, $post_id ) {

		switch ( $column ) {
			case 'timeline-years':
				$term = get_the_terms( $post_id, 'timeline_year', true );
				echo (int)$term[0]->name;
			break;
			case 'modified-last':

				$modified_date   = the_modified_date( 'Y/m/d - g:i A' );
				echo $modified_date;
			break;
		}

	}

	// Make Sortable
	add_filter( 'manage_edit-timeline_sortable_columns', 'sortable_timeline_column' );
	function sortable_timeline_column( $columns ) {

		$columns[ 'timeline-years' ]	= 'year';
		$columns['modified-last']		= 'modified';
		return $columns;

	}

	// Create Year Sortability
	add_action( 'pre_get_posts', 'timeline_year_orderby' );
	function timeline_year_orderby( $query ) {
		if( ! is_admin() ) return;

		if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
			switch( $orderby ) {
				case 'timeline_year':
					$query->set( 'meta_key', 'timeline_year_sort' );
					$query->set( 'orderby', 'meta_value_num' );
					break;
			}
		}
	}