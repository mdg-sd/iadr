<?php

// ADVANCED CUSTOM FIELDS
// ======================

// OPTIONS PAGES
// -------------

add_action( 'init', 'mdgcore_define_acf_options_pages', 0 );

function mdgcore_define_acf_options_pages() {

	if ( current_user_can('administrator') && function_exists('acf_add_options_page') ) {
		
		// acf_add_options_page([
		// 	'page_title' 	=> 'mdg Framework Options',
		// 	'menu_title'	=> 'mdg Framework',
		// 	'menu_slug' 	=> 'mdgcore-settings',
		// 	'capability'	=> 'edit_posts',
		// 	'redirect'		=> true
		// ]);

		acf_add_options_page([
			'page_title' 	=> 'mdg Framework Options',
			'menu_title'	=> 'mdg Framework',
			'parent_slug' 	=> 'options-general.php',
			// 'parent_slug' 	=> 'mdgcore-settings',
			'menu_slug' 	=> 'mdgcore-settings-general',
		]);

	}

}

// FIELD DEFINITIONS
// -----------------

// LOAD PATH

	add_filter('acf/settings/load_json', 'mdgcore_acf_json_load_point');

	function mdgcore_acf_json_load_point( $paths ) {

		unset($paths[0]);
	    
	    $paths[] = MDGCORE_PATH . 'custom/fields'; // higher priority
	    $paths[] = MDGCORE_PATH . 'fields';

	    return $paths;
	    
	}

// SAVE PATH (DEVELOPMENT MODE)

	add_filter('acf/settings/save_json', 'mdgcore_acf_json_save_point');
	 
	function mdgcore_acf_json_save_point( $path ) {

		if ( get_field('site_enable_core_development_mode', 'option') ) {

			$path = MDGCORE_PATH . 'fields';

		} else {

			$path = MDGCORE_PATH . 'custom/fields';

		}

	    return $path;
	    
	}
