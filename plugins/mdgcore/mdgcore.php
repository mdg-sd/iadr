<?php

/*
Plugin Name: mdg Core
Plugin URI: https://www.mdg.agency
Version: 2018.06.0
Author: mdg
Author URI: https://www.mdg.agency
Description: A collection of common resources and customization options for WordPress websites.
License: All rights reserved.
License URI: https://www.mdg.agency
Text Domain: mdgcore
*/

define( 'MDGCORE_PATH', plugin_dir_path( __FILE__ ) );
define( 'MDGCORE_URL',  plugin_dir_url(  __FILE__ ) );

// SETUP
// -----

	// Check whether plugin requirements are present & active. (Currently this is just the Advanced Custom Fields plugin.)
	include( MDGCORE_PATH . 'setup/setup.php');

	if ( !defined('MDGCORE_ACTIVE') ) return; // --> Setup failed. Stop loading plugin.

// FIELDS
// ------

	// Set the load & save locations for ACF field definitions.
	include( MDGCORE_PATH . 'fields/fields.php');

// CUSTOM
// ------

	// All site-specific customizations should be included via "custom/custom.php". All custom files should likewise be placed inside the "custom" folder. This directory should be excluded from core plugin updates. It must also contain the "fields" folder, which is loaded by fields.php and overrides the default ACF field definitions.
	include( MDGCORE_PATH . 'custom/custom.php');

// All PHP functions defined after this point should be contained inside a "function_exists" conditional. This allows them to be overridden inside custom.php.

// FUNCTIONS
// ---------

	// General/universal functions used by various components.
	include( MDGCORE_PATH . 'functions.php');

// BLOCKS
// -------

	include( MDGCORE_PATH . 'blocks/_blocks.php');
	include( MDGCORE_PATH . 'blocks/shared.php');
	include( MDGCORE_PATH . 'blocks/normal.php');
	include( MDGCORE_PATH . 'blocks/callout.php');
	include( MDGCORE_PATH . 'blocks/heading.php');
	include( MDGCORE_PATH . 'blocks/columns.php');
	include( MDGCORE_PATH . 'blocks/tiles.php');
	include( MDGCORE_PATH . 'blocks/actions.php');
	include( MDGCORE_PATH . 'blocks/divider.php');
	include( MDGCORE_PATH . 'blocks/feed.php');

// CONTENT
// -------

	include( MDGCORE_PATH . 'content/event.php');
	include( MDGCORE_PATH . 'content/profile.php');
	include( MDGCORE_PATH . 'content/ad.php');

// EDITING
// -------

	include( MDGCORE_PATH . 'features/breadcrumbs.php');
	include( MDGCORE_PATH . 'features/secondary_nav.php');
	include( MDGCORE_PATH . 'features/extended_page_attributes.php');
	include( MDGCORE_PATH . 'features/extended_thumbnail_attributes.php');
	include( MDGCORE_PATH . 'features/page_taxonomies.php');
	include( MDGCORE_PATH . 'features/colors.php');
	include( MDGCORE_PATH . 'features/image_sizes.php');
	include( MDGCORE_PATH . 'features/media.php');
	include( MDGCORE_PATH . 'features/shortcodes.php');

// ADVANCED
// --------

	include( MDGCORE_PATH . 'features/css.php');
	include( MDGCORE_PATH . 'features/libraries.php');
	include( MDGCORE_PATH . 'features/tracking_tags.php');
	include( MDGCORE_PATH . 'features/email.php');
	include( MDGCORE_PATH . 'features/html_widget_shortcodes.php');
	include( MDGCORE_PATH . 'features/default_custom_fields.php');
