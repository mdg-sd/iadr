<?php

/*
Plugin Name: Portal
Plugin URI: https://www.kaelri.com
Version: 2018.06
Author: Michael Engard
Author URI: https://www.kaelri.com
Description: A tool that enables seamless communication between server-side PHP and client-side JavaScript.
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: portal
*/

// OPTIONS
// -------

	add_action( 'init', 'portal_create_options_page' );

	function portal_create_options_page() {

		if ( current_user_can('administrator') && function_exists('acf_add_options_page') ) {
			
			acf_add_options_page([
				'page_title' 	=> 'Portal Options',
				'menu_title'	=> 'Portal',
				'parent_slug' 	=> 'options-general.php',
				'menu_slug' 	=> 'portal-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false
			]);

		}

	}

// REGISTER JS
// -----------

	add_action( 'wp_enqueue_scripts', 'portal_scripts' );

	function portal_scripts() {

		wp_register_script ( 'portal', plugin_dir_url( __FILE__ ) . 'portal.js', ['jquery'] );

	}

// CALL
// ----

	// Make a Portal-style API call from within WordPress (PHP). Mirrors the corresponding JavaScript function. Automatically formats & logs error reports.

	class PortalCall {

		public $url;
		public $data;
		public $response;
		public $output;
		public $has_error = false;
		
		function __construct( $url, $data = [] ) {

			$this->url  = $url;
			$this->data = $data;

			$this->send();

		}

		public function send() {

			// SEND
			$this->response = wp_safe_remote_post( $this->url, [
				'headers' => [ 'Content-Type' => 'application/json' ],
				'body'    => json_encode( $this->data ),
				'timeout' => 300,
			]);

			// ERROR CASE: HTTP POST FAIL
			if ( is_wp_error( $this->response ) ) {
				$this->output = portal_error('remote_post_fail', [
					'url'      => $this->url,
					'data'     => $this->data,
					'wp_error' => $this->response
				]);
				$this->has_error = true;
				return;
			}

			// GET OUTPUT
			$this->output = json_decode( $this->response['body'], true );

			// ERROR CASE: EMPTY OUTPUT
			if ( empty($this->output) || !is_array($this->output) ) {
				$this->output = portal_error('empty_output', [
					'url'      => $this->url,
					'data'     => $this->data,
					'raw'      => $this->response['body']
				]);
				$this->has_error = true;
				return;
			}

			// ERROR CASE: SERVER-SIDE ERROR
			// Error has already been generated server-side. Pass it through.
			if ( isset( $this->output['error'] ) ) {
				portal_error_log( $this->output['error'] );
				$this->has_error = true;
				return;
			}

		}

	}

// CREATE, ADD TO, OR CHANGE INITIAL CLIENT-SIDE DATA
// --------------------------------------------------

	function set_portal_data( $input, $value = null ) {

		global $portal_data;

		if ( !isset($portal_data) ) $portal_data = [];

		// Allow the input to be an array of key-value pairs, or just a string key.
		switch (gettype($input)) {

			// ARRAY
			case 'array':
				foreach ($input as $key => $value) {
					$portal_data[$key] = $value;
				}
				break;
			
			// STRING
			case 'string':
				$key = $input;
				$portal_data[$key] = $value;
				break;
			
			default: break;
		}

	}

	function get_portal_data() {

		global $portal_data;

		if ( !isset($portal_data) ) $portal_data = [];

		return $portal_data;
		
	}

// OUTPUT INITIAL CLIENT DATA IN FOOTER
// ------------------------------------

	add_action( 'wp_footer', 'portal_footer' );

	function portal_footer() {

		set_portal_data( 'url', home_url('/wp-json/') );

		$data     = get_portal_data();
		$json     = json_encode($data);
		$template = '<script>window.portalData = %s</script>';
		$html     = sprintf( $template, $json );

		echo $html;

	}

// UTILITY: GENERATE ERROR REPORT
// ------------------------------

	function portal_error( $code, $details = [], $status = 500 ) {

		$error = [
			'code'    => $code,
			'url'     => home_url( $_SERVER['REQUEST_URI'] ),
			'date'    => date('Y-m-d H:i:s'),
			'details' => $details,
		];

		portal_error_log( $error );

		$response = new WP_REST_Response([ 'error' => $error ]);
		$response->set_status( 500 );
		return $response;

	}

	function portal_error_log( $error ) {
		error_log( "\n" . 'Portal Error Report: ' . var_export($error, true) . "\n" );
	}

// LOADING SPINNER
// ---------------

	add_action( 'wp_head', 'portal_loading_spinner_css' );

	function portal_loading_spinner_css() {

		?><style>

			#portal-loading {
				position: fixed;
				opacity: 0;
				z-index: 300;
				top: 0;
				right: 0;
				bottom: 0;
				left: 0;
				background: rgba(0,0,0,0.6);
				color: white;
				display: -webkit-flex;
				display: flex;
				-webkit-justify-content: center;
				justify-content: center;
				-webkit-align-items: center;
				align-items: center;
				pointer-events: none;
				font-size: 6rem;
				-moz-transition: opacity .2s;
				-o-transition: opacity .2s;
				-webkit-transition: opacity .2s;
				transition: opacity .2s;
			}

			#portal-loading.active{
				opacity: 1;
				pointer-events: auto;
			}

		</style><?php
	}

	add_action( 'wp_footer', 'portal_loading_spinner_html' );

	function portal_loading_spinner_html() {

		?><div id="portal-loading"><i class="fa fa-cog fa-spin"></i></div><?php

	}

// EXAMPLE MODULE
// --------------

	$enable_example = get_field('portal_enable_example', 'option');
	if ( $enable_example ) include( plugin_dir_path( __FILE__ ) . 'example/example.php');
