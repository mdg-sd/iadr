// PORTAL
// ======

// FUNCTION: GET INITIAL CLIENT DATA
// ---------------------------------

	getPortalData = function( key ) {
		return window.portalData[key];
	}

// CLASS: CALL
// -----------

	PortalCall = function( config ) {

		this.url      = config.url      || portalData.url;
		this.method   = config.method   || '';
		this.data     = config.data     || {};
		this.callback = config.callback || function(){};
		this.silent   = config.silent   || true;
		this.response = null;

		this.send = function() {

			var self = this;

			if ( !this.silent ) this.showLoading();

			jQuery.ajax({
				type:     "POST",
				url:      this.url + this.method,
				headers:  { 'Content-Type': 'application/json' },
				data:     JSON.stringify( this.data ),
				success:  function( response ) {
					if ( !self.silent ) self.hideLoading();
					if ( response.error ) self.logError( response.error );
					if ( self.callback ) self.callback( response );
				},
				failure:  function( response ) {
					if ( !self.silent ) self.hideLoading();
					response = self.generateError( 'ajax_fail', { 'ajax_response': response } );
					if ( self.callback ) self.callback( response );
				},
				timeout: 300000
			});

		}

		this.generateError = function( code, details ) {

			var error = {
				'code': $code,
			};

			if ( details ) error.concat( details );

			this.logError( error );
			return { 'error': error };

		}

		this.logError = function( error ) {
			console.log( 'Portal Error Report', error );
		}

		this.showLoading = function() {
			jQuery('#portal-loading').addClass('active');
		}

		this.hideLoading = function() {
			jQuery('#portal-loading').removeClass('active');
		}

		return this;

	}
