jQuery(document).ready(function(){

	portalExample = new Vue({
		
		el: '#portal-example',

		data: {
			aTextField: '',
		},

		methods: {

			// PING
			ping: function() {

				var input = {
					aTextField: this.aTextField,
					aString: 'Zephyrs and stuff.',
					anInt: 17,
					aFloat: 3.59,
					anArray: [
						1,
						2,
						4,
						'Excellent',
						{
							alpha: 'beta',
							gamma: 'delta',
							epsilon: 9,
						}
					]
				}

				console.log( 'sent: ', input );
				
				portal.call({
					name:     'portal_example_ajax',
					input:    input,
					callback: this.pingCallback
				});

			},

			pingCallback: function( output ) {
				window.alert( output.aMessage );
				console.log( 'received: ', output );
			}

		}

	}); // portalExample

});
