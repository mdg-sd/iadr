<?php

// PORTAL EXAMPLE MODULE
// =====================

// SCRIPT
// ------

	add_action( 'wp_enqueue_scripts', 'portal_example_scripts' );

	function portal_example_scripts() {

		// App
		wp_enqueue_script( 'portal-example', plugin_dir_url( __FILE__ ) . 'example.js', ['jquery', 'portal', 'vue'] );

	}

// SHORTCODE
// ---------

	add_shortcode( 'portal_example', 'portal_example_html' );

	function portal_example_html($atts, $content = null) {
	    return file_get_contents( plugin_dir_path( __FILE__ ) . 'example.html' );
	}

// AJAX
// ----

	register_portal_callable_function( 'portal_example_ajax' );

	function portal_example_ajax( $input ) {

		$output = $input;

		$output['aMessage'] = 'Working! The input was: ' . $input['aTextField'];

		return $output;

	}
