<?php

// SHORTCODES
// ==========

// LINK
// ----

	// [link id="123"]
	// [link id="123"]text[/link]

	add_shortcode('link', 'mdgcore_shortcode_link');

	if ( !function_exists('mdgcore_shortcode_link') ) {
	function mdgcore_shortcode_link($atts, $content = null) {

		extract(shortcode_atts([
			'id' => 1
	    ], $atts));

	    $url = get_permalink($id);
	    
	    if ( !empty($content) ) {
	        return "<a href=\"$url\">$content</a>";
	    } else {
		   return get_permalink($id);
		}
	}}
