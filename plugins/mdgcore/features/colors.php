<?php

// COLORS
// ======

add_filter('tiny_mce_before_init', 'mdgcore_add_custom_colors');

if ( !function_exists('mdgcore_add_custom_colors') ) {
function mdgcore_add_custom_colors($init) {

	$colors = get_field('site_add_custom_colors', 'option');

	if ( !empty($colors) ) {

		$color_map = [];

		foreach ($colors as $color) {

			$color_map[] = substr( $color['hex'], 1);
			$color_map[] = $color['name'];
		
		}

		$init['textcolor_map'] = json_encode($color_map);

	}

	return $init;

}}
