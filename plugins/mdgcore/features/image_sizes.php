<?php

// IMAGE SIZES
// ===========

// REGISTER IMAGE SIZE
// -------------------

	add_action( 'after_setup_theme', 'mdgcore_add_image_sizes' );

	if ( !function_exists('mdgcore_add_image_sizes') ) {
	function mdgcore_add_image_sizes() {

		$custom_sizes = get_field('site_add_image_sizes', 'option');

		if ( !empty($custom_sizes) ) { foreach ($custom_sizes as $size) {
			
			$name   = $size['name'];
			$slug   = sanitize_title($name);
			$width  = !empty($size['max_width'] ) ? $size['max_width']  : 9999;
			$height = !empty($size['max_height']) ? $size['max_height'] : 9999;
			$crop   = $size['crop'];

			add_image_size( $slug, $width, $height, $crop  );

		}}

	}}

// MAKE IMAGE SIZE SELECTABLE IN WYSIWYG
// -------------------------------------

	add_filter( 'image_size_names_choose', 'mdgcore_name_image_sizes' );
 
	if ( !function_exists('mdgcore_name_image_sizes') ) {
	function mdgcore_name_image_sizes( $sizes ) {

		$custom_sizes = get_field('site_add_image_sizes', 'option');

		if ( !empty($custom_sizes) ) { foreach ($custom_sizes as $size) {
			
			$name   = $size['name'];
			$slug   = sanitize_title($name);

			$sizes[ $slug ] = $name;

		}}

	    return $sizes;

	}}
