<?php

add_action( 'wp_enqueue_scripts', 'mdgcore_enqueue_css' );

if ( !function_exists('mdgcore_enqueue_css') ) {
function mdgcore_enqueue_css() {

	$enable_css = get_field('site_enable_core_css', 'option');
	if ( !$enable_css ) return;

	$css_mod_time = filemtime( plugin_dir_path( __FILE__ ) . '../css/mdgcore.css' );

	wp_enqueue_style ( 'mdgcore', plugin_dir_url( __FILE__ ) . '../css/mdgcore.css', false, $css_mod_time );

}}
