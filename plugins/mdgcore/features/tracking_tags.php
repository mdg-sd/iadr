<?php

// TRACKING TAGS
// =============

// SETUP
// -----

	add_action( 'init', 'mdgcore_tracking_tags_setup', 0 );

	if ( !function_exists('mdgcore_tracking_tags_setup') ) {
	function mdgcore_tracking_tags_setup() {

		if ( !get_field('site_enable_tracking_tags', 'option') ) return;

		if ( current_user_can('administrator') && function_exists('acf_add_options_page') ) {
			
			acf_add_options_page([
				'page_title' 	=> 'Tags & Scripts',
				'menu_title'	=> 'Tags & Scripts',
				'parent_slug' 	=> 'options-general.php',
				'menu_slug' 	=> 'mdgcore-settings-tags',
			]);
		}

		add_action( 'wp_head', 'mdgcore_do_header_tags' );
		add_action( 'wp_footer', 'mdgcore_do_footer_tags' );

	}}

// HEADER TAGS
// -----------

	add_action( 'wp_head', 'mdgcore_do_header_tags' );

	if ( !function_exists('mdgcore_do_header_tags') ) {
	function mdgcore_do_header_tags() {

		if ( !get_field('site_enable_tracking_tags', 'option') ) return;

		if (have_rows('site_tags_head', 'option')) { while (have_rows('site_tags_head', 'option')) {  the_row();

			if ( !get_sub_field('enabled') ) continue;

			?>

			<!-- <?=strtoupper( get_sub_field('label') )?> -->
			<?=get_sub_field('html')?>

		<?php }}

	}}

// BODY TAGS
// ---------

	if ( !function_exists('mdgcore_the_body_tags') ) {
	function mdgcore_the_body_tags() {

		if (have_rows('site_tags_body', 'option')) { while (have_rows('site_tags_body', 'option')) {  the_row(); ?>

			<!-- <?=strtoupper( get_sub_field('label') )?> -->
			<?=get_sub_field('html')?>

		<?php }}

	}}

// FOOTER TAGS
// -----------

	if ( !function_exists('mdgcore_do_footer_tags') ) {
	function mdgcore_do_footer_tags() {

		if (have_rows('site_tags_footer', 'option')) { while (have_rows('site_tags_footer', 'option')) {  the_row(); ?>

			<!-- <?=strtoupper( get_sub_field('label') )?> -->
			<?=get_sub_field('html')?>

		<?php }}

	}}
