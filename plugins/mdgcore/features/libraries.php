<?php

// LIBRARIES
// =========

add_action( 'wp_enqueue_scripts', 'mdgcore_enqueue_libraries' );

if ( !function_exists('mdgcore_enqueue_libraries') ) {
function mdgcore_enqueue_libraries() {

	$enabled_libraries = get_field('site_enable_libraries', 'option') ?: [];

	// JQUERY
	if (in_array('jquery', $enabled_libraries)) {
		wp_enqueue_script ( 'jquery' );
	}

	// JQUERY UI
	if (in_array('jquery_ui', $enabled_libraries)) {
		wp_enqueue_style  ( 'jquery_ui_css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css' );
		wp_enqueue_script ( 'jquery_ui_js',  'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
	}

	// ANGULAR
	if (in_array('angular', $enabled_libraries)) {
		//+ Actually add Angular.
	}

	// BOOTSTRAP
	if (in_array('bootstrap', $enabled_libraries)) {

		wp_enqueue_style  ( 'bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' );
		wp_enqueue_script ( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' );
		wp_enqueue_script ( 'bootstrap-js',  'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', ['jquery', 'popper'] );

	}

	// BOOTBOX
	if (in_array('bootbox', $enabled_libraries)) {

		wp_enqueue_script ( 'bootbox', MDGCORE_URL . 'lib/bootbox/bootbox.min.js' );

	}

	// FONT_AWESOME 4
	if (in_array('font_awesome_4', $enabled_libraries)) {
		wp_enqueue_style ( 'font-awesome-4', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	}

	// FONT_AWESOME 5
	if (in_array('font_awesome_5', $enabled_libraries)) {
		wp_enqueue_script ( 'font-awesome-5', 'https://use.fontawesome.com/releases/v5.0.9/js/all.js' );
	}

	// LIGHTBOX
	if (in_array('lightbox', $enabled_libraries)) {
		wp_enqueue_style  ( 'lightbox', MDGCORE_URL . 'lib/lightbox/css/lightbox.css' );
		wp_enqueue_script ( 'lightbox', MDGCORE_URL . 'lib/lightbox/js/lightbox.js', null, null, true );
	}

	// MOMENT.JS
	if (in_array('moment', $enabled_libraries)) {
		wp_enqueue_script( 'moment-js', '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.min.js' );
	}

	// PRISM
	if (in_array('prism', $enabled_libraries)) {
		wp_enqueue_style  ( 'prism', MDGCORE_URL . 'lib/prism/prism.css' );
		wp_enqueue_script ( 'prism', MDGCORE_URL . 'lib/prism/prism.js', null, null, true );
	}

	// SLICK
	if (in_array('slick', $enabled_libraries)) {
		wp_enqueue_style  ( 'slick',       '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css' );
		wp_enqueue_script ( 'slick',       '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery') );
	}

	// SLICK THEME
	if (in_array('slick_theme', $enabled_libraries)) {

		wp_enqueue_style  ( 'slick-theme', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css' );

	}

	// VUE
	if (in_array('vue', $enabled_libraries)) {

		$vue_url = ( defined('WP_DEBUG') && WP_DEBUG ) ? 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js' : 'https://cdn.jsdelivr.net/npm/vue';

		wp_enqueue_script ( 'vue', $vue_url );

	}

	// WAYPOINTS
	if (in_array('waypoints', $enabled_libraries)) {
		wp_enqueue_script ( 'waypoints', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js' );
	}

}}

// PORTAL
// Portal requires init-level integration with WordPress.

add_action( 'init', 'mdgcore_enable_portal', 1 );

if ( !function_exists('mdgcore_enable_portal') ) {
function mdgcore_enable_portal() {

	$enabled_libraries = get_field('site_enable_libraries', 'option') ?: [];

	if ( !in_array('portal', $enabled_libraries) ) return;

	include( plugin_dir_path( __FILE__ ) . 'portal/portal.php' );

}}
