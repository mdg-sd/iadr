<?php

add_filter('upload_mimes', 'mdgcore_add_media_types');

if ( !function_exists('mdgcore_add_media_types') ) {
function mdgcore_add_media_types($mimes) {

	$types = get_field('site_add_media_types', 'option');

	if ( is_array($types) ) { foreach ($types as $type) {
		
		$extension = $type['ext'];
		$mime      = $type['mime'];

		$mimes[ $extension ] = $mime;

	}}

	return $mimes;

}}
