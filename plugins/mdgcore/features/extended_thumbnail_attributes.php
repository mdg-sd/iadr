<?php

add_action('admin_head', 'mdgcore_hide_extended_thumbnail_attributes');

if ( !function_exists('mdgcore_hide_extended_thumbnail_attributes') ) {
function mdgcore_hide_extended_thumbnail_attributes() {

	$enable_thumbnail_attributes = get_field('site_extend_thumbnail_attributes', 'option');

	if ( !$enable_thumbnail_attributes ) {

		?><style>
			/* This field group is hidden because the "Extended Thumbnail Attributes" option under "Customizations" is disabled. */
			#acf-group_59e137c3cf6ae { display: none; }
		</style><?php

	}

}}

if ( !function_exists('mdgcore_get_extended_thumbnail_attributes') ) {
function mdgcore_get_extended_thumbnail_attributes() {

	$attrs = [];

	$attrs['style'] = get_field('page_image_style');

	switch ( $attrs['style'] ) {
		case 'alignright':
			$attrs['size']  = 'medium';
			$attrs['class'] = 'alignright';
			break;
		case 'alignleft':
			$attrs['size']  = 'medium';
			$attrs['class'] = 'alignleft';
			break;
		case 'aligncenter':
			$attrs['size']  = 'full';
			$attrs['class'] = 'aligncenter';
			break;
		case 'alignnone':
			$attrs['size']  = 'full';
			$attrs['class'] = 'alignnone';
			break;
		default:
			$attrs['size']  = null;
			$attrs['class'] = 'default';
			break;
	}

	return $attrs;

}}
