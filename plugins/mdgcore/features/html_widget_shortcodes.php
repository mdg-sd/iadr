<?php

add_action( 'init', 'mdgcore_enable_html_widget_shortcodes' );

if ( !function_exists('mdgcore_enable_html_widget_shortcodes') ) {
function mdgcore_enable_html_widget_shortcodes() {

	$enable_html_widget_shortcodes = get_field('site_enable_html_widget_shortcodes', 'option');
	if ( !$enable_html_widget_shortcodes ) return;

	add_filter('widget_custom_html_content','do_shortcode');

}}
