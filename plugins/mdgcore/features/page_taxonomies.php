<?php

// ENABLE POST CATEGORIES & TAGS ON PAGES
// ======================================

add_action( 'init', 'mdgcore_enable_page_taxonomies' );

if ( !function_exists('mdgcore_enable_page_taxonomies') ) {
function mdgcore_enable_page_taxonomies() {

	$enabled_taxonomies = get_field('site_enable_page_taxonomies', 'option');

	if ( empty($enabled_taxonomies) || !is_array($enabled_taxonomies) ) return;

	if ( in_array('categories', $enabled_taxonomies ) ) {
		register_taxonomy_for_object_type( 'category', 'page'    );
	}

	if ( in_array('tags', $enabled_taxonomies ) ) {
		register_taxonomy_for_object_type( 'post_tag', 'page'   );
	}

}}
