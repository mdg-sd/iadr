<?php

add_action('admin_head', 'mdgcore_hide_extended_page_attributes');

if ( !function_exists('mdgcore_hide_extended_page_attributes') ) {
function mdgcore_hide_extended_page_attributes() {

	$enable_page_attributes = get_field('site_extend_page_attributes', 'option');

	if ( !$enable_page_attributes ) {

		?><style>
			/* This field group is hidden because the "Extended Page Attributes" option under "Customizations" is disabled. */
			#acf-group_59ce7ea146d7c { display: none; }
		</style><?php

	}

}}

if ( !function_exists('mdgcore_get_display_title') ) {
function mdgcore_get_display_title( $source = null ) {

	$source = get_post($source);

	return get_field('page_display_title', $source) ?: get_the_title($source);

}}

if ( !function_exists('mdgcore_hide_title') ) {
function mdgcore_hide_title( $source = null ) {

	$source = get_post($source);

	return get_field('page_hide_title', $source);

}}

if ( !function_exists('mdgcore_get_subtitle') ) {
function mdgcore_get_subtitle( $source = null ) {

	$source = get_post($source);

	return get_field('page_subtitle', $source);

}}
