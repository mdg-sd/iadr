<?php

// RESTORE DEFAULT WORDPRESS CUSTOM FIELDS BOX
// ===========================================

add_action( 'init', 'mdgcore_enable_default_custom_fields' );

if ( !function_exists('mdgcore_enable_default_custom_fields') ) {
function mdgcore_enable_default_custom_fields() {

	$enable_cf = get_field('site_enable_custom_fields', 'option');
	if ( !$enable_cf ) return;

	add_filter('acf/settings/remove_wp_meta_box', '__return_false');

}}
