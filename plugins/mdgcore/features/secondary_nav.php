<?php

// SECONDARY NAV
// =============

// SETUP
// -----

    add_action( 'init', 'mdgcore_secondary_nav_setup', 0 );

    if ( !function_exists('mdgcore_secondary_nav_setup') ) {
    function mdgcore_secondary_nav_setup() {

        if ( !get_field('site_enable_secondary_nav', 'option') ) return;

		add_action( 'widgets_init', function(){
			register_widget( 'mdgcore_Secondary_Nav_Widget' );
		});

    }}

// FIELDS
// ------

	add_action('admin_head', 'mdgcore_hide_secondary_nav_fields');

	if ( !function_exists('mdgcore_hide_secondary_nav_fields') ) {
	function mdgcore_hide_secondary_nav_fields() {

		$enable_secondary_nav = get_field('site_enable_secondary_nav', 'option');

		if ( !$enable_secondary_nav ) {

			?><style>
				/* This field group is hidden because the "Secondary Navigation" option under "Customizations" is disabled. */
				#acf-group_586fbb00d5672 { display: none; }
			</style><?php

		}

	}}

// WIDGET
// ------

	if ( !class_exists('mdgcore_Secondary_Nav_Widget') ) {
	class mdgcore_Secondary_Nav_Widget extends WP_Widget {

		// SETUP
		public function __construct() {

			$widget_ops = array(
				'classname' => 'mdgcore-secondary-nav-widget',
				'description' => 'Custom sidebar menu for each page.',
			);
			parent::__construct( 'mdgcore_secondary_nav_widget', 'Secondary Navigation', $widget_ops );
		}

		// OUTPUT
		public function widget( $args, $instance ) {

			$nav_items = array();

			global $post;
			$nav_source = $post;

			$virtual_post_type = mdgcore_get_virtual_post_type();

			$nav_structure = get_field('secondary_nav_structure');

			switch ($virtual_post_type) {

				default:

					if ( empty($nav_structure) || $nav_structure == 'none' ) {
						return;
					}

					// COPY FROM ANOTHER PAGE
					// Recurses through "copy from" source pages until a page with its own secondary nav is found.
					while ( $nav_structure == 'copy' ) {
						$nav_source    = get_field('secondary_nav_source',    $nav_source);
						$nav_structure = get_field('secondary_nav_structure', $nav_source);
					}

					// CHILDREN & SIBLINGS
					if ( $nav_structure == 'children' || $nav_structure == 'siblings' ) {

						$nav_start = get_field('secondary_nav_start', $nav_source);
						$nav_start = !empty($nav_start) ? $nav_start : $nav_source;

						// The process for "siblings" is identical to children. We just use the parent of the start page instead of the start page itself. If we try to get children and find none, we fall back to siblings.
						$nav_children = [];

						if ( $nav_structure == 'children' ) {

							$nav_start_id = $nav_start->ID;

							$nav_children = get_posts( array(
								'post_type'   => 'page',
								'post_parent' => $nav_start_id,
								'post_status' => 'publish',
		    					'orderby'     => 'menu_order',
		    					'order'       => 'ASC',
		    					'numberposts' => -1
							));

						}

						if ( $nav_structure == 'siblings' || empty($nav_children) ) {

							$nav_start_id = $nav_start->post_parent;

							$nav_children = get_posts( array(
								'post_type'   => 'page',
								'post_parent' => $nav_start_id,
								'post_status' => 'publish',
		    					'orderby'     => 'menu_order',
		    					'order'       => 'ASC',
		    					'numberposts' => -1
							));

						}

						if ( !empty($nav_children) ) { foreach ( $nav_children as $child ) {

							$nav_item = array(
								'text'   => $child->post_title,
								'url'    => get_permalink( $child->ID ),
								'target' => '',
							);

							$nav_items[] = $nav_item;

						}}

						//+ Add recursive function for depth.

					}

					// CUSTOM
					elseif ( $nav_structure == 'custom' ) {

						if ( have_rows('secondary_nav_links') ) { while( have_rows('secondary_nav_links') ) { the_row();

							$custom_link = get_sub_field('link');

							$nav_item = array(
								'text'   => $custom_link['title'],
								'url'    => $custom_link['url'],
								'target' => $custom_link['target'],
							);

							$nav_items[] = $nav_item;

						}}

					}

					break;

			} // switch virtual_post_type

			// DISPLAY
			if ( empty($nav_items) ) {
				return;
			}

			echo $args['before_widget'];

			// TITLE
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}

			?>

			<ul>
			<?php foreach ($nav_items as $nav_item) { ?>

				<li>
					<i class="widget-toggle fa fa-caret-right"></i>
					<a href="<?=esc_url($nav_item['url'])?>" target="<?=$nav_item['target']?>"><?=esc_html($nav_item['text'])?></a>
				</li>

			<?php } ?>
			</ul>

			<?php

			echo $args['after_widget'];

		}

		// FORM
		public function form( $instance ) {

			$title = ! empty( $instance['title'] ) ? $instance['title'] : '';

			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>

			<?php
		}

		// UPDATE
		public function update( $new_instance, $old_instance ) {

			$instance = array();

			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			return $instance;

		}

	}}
