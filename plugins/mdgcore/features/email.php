<?php

// EMAIL OVERRIDES
// ===============

// SETUP
// -----

	add_action( 'init', 'mdgcore_email_overrides_setup', 0 );

	if ( !function_exists('mdgcore_email_overrides_setup') ) {
	function mdgcore_email_overrides_setup() {

		if ( !get_field('site_enable_email_overrides', 'option') ) return;

		if ( current_user_can('administrator') && function_exists('acf_add_options_page') ) {

			acf_add_options_page([
				'page_title'    => 'Email Notification Settings',
				'menu_title'    => 'Email',
				'parent_slug' 	=> 'options-general.php',
				'menu_slug'     => 'mdgcore-settings-email',
			]);

		}

		add_action( 'phpmailer_init', 'mdgcore_configure_email' );

	}}

// FILTER
// ------

	if ( !function_exists('mdgcore_configure_email') ) {
	function mdgcore_configure_email( PHPMailer $phpmailer ){

		// SMTP
		$is_smtp = get_field( 'site_email_is_smtp', 'option' );

		if ( $is_smtp ) {
			$phpmailer->isSMTP();
		}

		// FROM NAME
		$from_name = get_field( 'site_email_from_name', 'option' );

		if ( !empty($from_name) ) {
			$phpmailer->FromName=$from_name;
		}

		// FROM ADDRESS
		$from_address = get_field( 'site_email_from_address', 'option' );

		if ( !empty($from_address) ) {
			$phpmailer->From = $from_address;
		}

		// HOST
		$host = get_field( 'site_email_host', 'option' );

		if ( !empty($host) ) {
			$phpmailer->Host = $host;
		}

		// PORT
		$port = get_field( 'site_email_port', 'option' );

		if ( !empty($port) && $port != 'default' ) {
			$phpmailer->Port = $port;
		}

		//$phpmailer->SMTPDebug = 2;
		//$phpmailer->SMTPAuth = false;
		//$phpmailer->Username = '';
		//$phpmailer->Password = '';
		//$phpmailer->SMTPSecure = false;
		//$phpmailer->SMTPSecure = 'tls';

	}}
