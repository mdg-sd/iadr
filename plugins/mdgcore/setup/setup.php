<?php

// SETUP
// =====

// CHECK FOR ADVANCED CUSTOM FIELDS PRO PLUGIN
// -------------------------------------------

if ( !class_exists( 'ACF' ) ) {

	add_action( 'admin_notices', function (){

	    ?><div class="error notice">
	        <p>mdg Core requires the <a href="https://www.advancedcustomfields.com/" target="_blank">Advanced Custom Fields PRO</a> plugin to be installed and activated. <a href="<?=admin_url('plugins.php')?>">Go to Plugins &raquo;</a></p>
	    </div><?php

	});

	return; // --> mdgcore.php

}

// If all checks have succeeded, declare plugin as active.

define( 'MDGCORE_ACTIVE', true );
