<?php

// PROFILE
// =======

// Custom post type for individual or group profiles (e.g. speakers, presenters, organizers, staff).

// SETUP
// -----

	add_action( 'init', 'mdgcore_profile_setup', 0 );

	if ( !function_exists('mdgcore_profile_setup') ) {
	function mdgcore_profile_setup() {

		$enable_profile = get_field('site_enable_profile', 'option');
		if ( !$enable_profile ) return;

		add_action( 'init', 'mdgcore_regcpt_profile' );
		add_action( 'init', 'mdgcore_regctx_profile_category' );

	}}

// CUSTOM POST TYPE
// ----------------

	if ( !function_exists('mdgcore_regcpt_profile') ) {
	function mdgcore_regcpt_profile() {

		register_post_type( 'profile', [
			'supports'            => [ 'title', 'editor', 'thumbnail' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-groups',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Profiles',
				'singular_name'       => 'Profile',
				'menu_name'           => 'Profiles',
				'parent_item_colon'   => 'Parent Profile:',
				'all_items'           => 'All Profiles',
				'view_item'           => 'View Profiles',
				'add_new_item'        => 'Add New Profile',
				'add_new'             => 'New Profile',
				'edit_item'           => 'Edit Profile',
				'update_item'         => 'Update Profile',
				'search_items'        => 'Search Profiles',
				'not_found'           => 'No Profiles Found',
				'not_found_in_trash'  => 'No Profiles Found in Trash',
			],
		]);

	}}

// CUSTOM TAXONOMY: PROFILE CATEGORY
// ---------------------------------

	if ( !function_exists('mdgcore_regctx_profile_category') ) {
	function mdgcore_regctx_profile_category()  {

		register_taxonomy( 'profile_category', ['profile'], [
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => ['slug' => 'profile-category'],
			'labels'                     => [
				'name'                       => 'Profile Categories',
				'singular_name'              => 'Profile Category',
				'menu_name'                  => 'Categories',
				'all_items'                  => 'All Categories',
				'parent_item'                => 'Parent Category',
				'parent_item_colon'          => 'Parent Category:',
				'new_item_name'              => 'New Category',
				'add_new_item'               => 'Add New Category',
				'edit_item'                  => 'Edit Category',
				'update_item'                => 'Update Category',
				'separate_items_with_commas' => 'Separate Categories with Commas',
				'search_items'               => 'Search Categories',
				'add_or_remove_items'        => 'Add or Remove Categories',
				'choose_from_most_used'      => 'Choose from the most used Categories',
			],
		]);

	}}
