<?php

// EVENT
// =====

// Custom post type for timed events (e.g. meetings, sessions).

// SETUP
// -----

	add_action( 'init', 'mdgcore_event_setup', 0 );

	if ( !function_exists('mdgcore_event_setup') ) {
	function mdgcore_event_setup() {

		$enable_event = get_field('site_enable_event', 'option');
		if ( !$enable_event ) return;

		add_action( 'init', 'mdgcore_regcpt_event' );
		add_action( 'init', 'mdgcore_regctx_event_category' );

	}}

// CUSTOM POST TYPE
// ----------------

	if ( !function_exists('mdgcore_regcpt_event') ) {
	function mdgcore_regcpt_event() {

		register_post_type( 'event', [
			'supports'            => [ 'title', 'editor', 'thumbnail' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-calendar-alt',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Events',
				'singular_name'       => 'Event',
				'menu_name'           => 'Events',
				'parent_item_colon'   => 'Parent Event:',
				'all_items'           => 'All Events',
				'view_item'           => 'View Events',
				'add_new_item'        => 'Add New Event',
				'add_new'             => 'New Event',
				'edit_item'           => 'Edit Event',
				'update_item'         => 'Update Event',
				'search_items'        => 'Search Events',
				'not_found'           => 'No Events Found',
				'not_found_in_trash'  => 'No Events Found in Trash',
			],
		]);

	}}

// CUSTOM TAXONOMY: EVENT CATEGORY
// -------------------------------

	if ( !function_exists('mdgcore_regctx_event_category') ) {
	function mdgcore_regctx_event_category()  {

		register_taxonomy( 'event_category', ['event'], [
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => ['slug' => 'event-category'],
			'labels'                     => [
				'name'                       => 'Event Categories',
				'singular_name'              => 'Event Category',
				'menu_name'                  => 'Categories',
				'all_items'                  => 'All Categories',
				'parent_item'                => 'Parent Category',
				'parent_item_colon'          => 'Parent Category:',
				'new_item_name'              => 'New Category',
				'add_new_item'               => 'Add New Category',
				'edit_item'                  => 'Edit Category',
				'update_item'                => 'Update Category',
				'separate_items_with_commas' => 'Separate Categories with Commas',
				'search_items'               => 'Search Categories',
				'add_or_remove_items'        => 'Add or Remove Categories',
				'choose_from_most_used'      => 'Choose from the most used Categories',
			],
		]);

	}}
