<?php

// ADS
// ===

// Custom post type for advertisements.

// SETUP
// -----

	add_action( 'init', 'mdgcore_ad_setup', 0 );

	if ( !function_exists('mdgcore_ad_setup') ) {
	function mdgcore_ad_setup() {

		$enable_ad = get_field('site_enable_ad', 'option');
		if ( !$enable_ad ) return;

		add_action( 'init', 'mdgcore_regcpt_ad' );
		add_action( 'init', 'mdgcore_regctx_ad_slot' );

		add_shortcode('ad', 'mdgcore_shortcode_ad');

		add_action( 'widgets_init', function(){
			register_widget( 'MDGCORE_Ad_Slot_Widget' );
		});

	}}

// CUSTOM POST TYPE
// ----------------

	if ( !function_exists('mdgcore_regcpt_ad') ) {
	function mdgcore_regcpt_ad() {

		register_post_type( 'ad', [
			'supports'            => [ 'title', 'thumbnail' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-products',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Ads',
				'singular_name'       => 'Ad',
				'menu_name'           => 'Ads',
				'parent_item_colon'   => 'Parent Ad:',
				'all_items'           => 'All Ads',
				'view_item'           => 'View Ads',
				'add_new_item'        => 'Add New Ad',
				'add_new'             => 'New Ad',
				'edit_item'           => 'Edit Ad',
				'update_item'         => 'Update Ad',
				'search_items'        => 'Search Ads',
				'not_found'           => 'No Ads Found',
				'not_found_in_trash'  => 'No Ads Found in Trash',
			],
		]);

	}}

// CUSTOM TAXONOMY: AD SLOTS
// -------------------------

	if ( !function_exists('mdgcore_regctx_ad_slot') ) {
	function mdgcore_regctx_ad_slot()  {

		register_taxonomy( 'ad_slot', ['ad'], [
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => ['slug' => 'ad-slot'],
			'labels'                     => [
				'name'                       => 'Slots',
				'singular_name'              => 'Slot',
				'menu_name'                  => 'Slots',
				'all_items'                  => 'All Slots',
				'parent_item'                => 'Parent Slot',
				'parent_item_colon'          => 'Parent Slot:',
				'new_item_name'              => 'New Slot',
				'add_new_item'               => 'Add New Slot',
				'edit_item'                  => 'Edit Slot',
				'update_item'                => 'Update Slot',
				'separate_items_with_commas' => 'Separate Slots with Commas',
				'search_items'               => 'Search Slots',
				'add_or_remove_items'        => 'Add or Remove Slots',
				'choose_from_most_used'      => 'Choose from the most used Slots',
			],
		]);

	}}

// SHORTCODE: AD
// -------------

	if ( !function_exists('mdgcore_shortcode_ad') ) {
	function mdgcore_shortcode_ad($atts, $content = null) {

	    // Shortcode options.
		extract(shortcode_atts([
			'slot' => null,
			'id'   => null,
	    ], $atts));

	    // GET AD
	    $ad = null;

	    if ( isset($id) ) {

	    	$ad = get_post( $id );

	    	if ( !$ad ) return;

	    } else {

	    	// Get current post's tags.
			$tag_ids = [];
	        $tags    = get_terms('post_tag');
	        foreach($tags as $tag){
	            $tag_ids[] = $tag->term_id; 
	        }

	 		$ads = get_posts([
				'post_type'      => 'ad',
				'posts_per_page' => 1,
				'orderby'        => 'rand',
				'tax_query'      => [
					// 'relation' => 'AND',
					[
						'taxonomy' => 'ad_slot',
						'field'    => 'slug',
						'terms'    => $slot,
					],
					[
						'relation' => 'OR',
						[
							'taxonomy' => 'post_tag',
							'operator' => 'NOT EXISTS',
						],
						[
							'taxonomy' => 'post_tag',
							'field'    => 'term_id',
							'terms'    => $tag_ids,
						]
					],
				],
			]);

			if ( empty($ads) ) return;

			$ad = $ads[0];

	    }

	    $html = '<section class="ad-container">';
	    $html .= $ad->post_content;
		$html .= '</section>';

		return $html;

    }}

// WIDGET: AD SLOT

	// Class (based on https://codex.wordpress.org/Widgets_API)
	if ( !class_exists('MDGCORE_Ad_Slot_Widget') ) {
	class MDGCORE_Ad_Slot_Widget extends WP_Widget {

		// SETUP
		public function __construct() {

			$widget_ops = array(
				'classname'   => 'mdgcore-ad-slot-widget',
				'description' => 'Show a random ad from the selected ad slot.',
			);
			parent::__construct( 'MDGCORE_Ad_Slot_Widget', 'Ad Slot', $widget_ops );
		}

		// OUTPUT
		public function widget( $args, $instance ) {

			$html  = $args['before_widget'];
			$html .= do_shortcode( sprintf('[ad slot="%s"]', $instance['slot']) );
			$html .= $args['after_widget'];

			echo $html;

		}

		// FORM
		public function form( $instance ) {

			$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
			$slot  = ! empty( $instance['slot'] )  ? $instance['slot']  : '';

			?>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'slot' ) ); ?>"><?php _e( esc_attr( 'Slot:' ) ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'slot' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'slot' ) ); ?>" type="text" value="<?php echo esc_attr( $slot ); ?>">
			</p>

			<?php
		}

		// UPDATE
		public function update( $new_instance, $old_instance ) {

			$instance = array();

			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['slot']  = ( ! empty( $new_instance['slot'] ) )  ? strip_tags( $new_instance['slot'] )  : '';

			return $instance;

		}

	}} // MDGCORE_Ad_Slot_Widget
