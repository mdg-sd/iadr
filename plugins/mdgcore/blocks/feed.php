<?php

// BLOCK: FEED
// ===========

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_feed_setup', 0 );

	if ( !function_exists('mdgcore_block_feed_setup') ) {
	function mdgcore_block_feed_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('feed', 'mdgcore_render_block_feed');

		// SHORTCODE
		add_shortcode('feed', 'mdgcore_shortcode_feed');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_feed') ) {
	function mdgcore_render_block_feed ( $block ) {

	    include_once( ABSPATH . WPINC . '/feed.php' );

	    $feed = fetch_feed( $block['url']);

		if ( is_wp_error( $feed ) ) return;

	    $num = $feed->get_item_quantity( $block['num'] ); // Get number of feed items, with a ceiling of original $num.
	    if ( $num < 1 ) return;

	    $items = $feed->get_items( 0, $num );

		?>

		<section class="entry-feed">

			<div class="entry-block-inner">
			<?php foreach ($items as $item) { ?>

				<article class="entry-feed-item">

					<h1><a href="<?php echo esc_url( $item->get_permalink() ); ?>"><?=esc_html( $item->get_title() )?></a></h1>
					
					<p class="subtitle"><?=$item->get_date('j F Y')?></p>

					<p><?=$item->get_description()?></p>

				</article>

			<?php } ?>
			</div>

		</section>

		<?php

	}}

// SHORTCODE
// ---------

	if ( !function_exists('mdgcore_shortcode_feed') ) {
	function mdgcore_shortcode_feed($atts) {

	    extract(shortcode_atts([
			"url" => 'http://',
			"num" => '1',
	    ], $atts));

	    // Set up an ad-hoc "block" to render.
	    $block = [
	    	'url'   => $url,
	    	'num'   => $num,
	    ];

	    ob_start();

	    mdgcore_render_feed( $block );

		$output = ob_get_contents();

	    ob_end_clean();

	    return $output;

	}}
