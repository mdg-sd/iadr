<?php

// BLOCK: CALLOUT
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_callout_setup', 0 );

	if ( !function_exists('mdgcore_block_callout_setup') ) {
	function mdgcore_block_callout_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('callout', 'mdgcore_render_block_callout');

		// CALLOUT
		add_shortcode('callout', 'mdgcore_shortcode_callout');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_callout') ) {
	function mdgcore_render_callout ( $block ) {

		$callout_id = uniqid('entry-callout-');

		$custom = $block['customize'];
		if ( empty($custom) ) $custom = []; // Convert empty string to array for consistency.

		$background_color = in_array('background', $custom) ? $block['background_color'] : false;
		$text_color       = in_array('text',       $custom) ? $block['text_color']       : false;
		$link_color       = in_array('link',       $custom) ? $block['link_color']       : false;
		$align            = in_array('align',      $custom) ? $block['align']            : 'center';

		// Generate text color based on background color.
		if ( $background_color && !$text_color ) {
			$text_color = mdgcore_get_contrasting_colors($background_color);
		}

		if ( $background_color && !$link_color ) {
			$link_color = $text_color;
		}

		// ALIGNMENT
		$callout_class = 'entry-callout';
		if ( $align ) $callout_class .= ' align'.$align;

		?>

		<?php if ( !empty($custom) ) { ?>
		<style type="text/css" scoped="scoped">
			
			<?php if ( $background_color ) { ?>
			#<?=$callout_id?> {
				background: <?=$background_color?>;
			}
			<?php } ?>

			<?php if ( $text_color ) { ?>
			#<?=$callout_id?> {
				color: <?=$text_color?>;
			}
			<?php } ?>

			<?php if ( $link_color ) { ?>
			#<?=$callout_id?> a,
			#<?=$callout_id?> a:active,
			#<?=$callout_id?> a:link,
			#<?=$callout_id?> a:visited {
				color: <?=$link_color?>;
			}
			<?php } ?>

		</style>
		<?php } ?>

		<section class="<?=$callout_class?>" id="<?=$callout_id?>">
			<div class="entry-block-inner">

				<?php if ( !empty( $block['title'] ) ) {
					$heading = [ 'title' => $block['title'] ];
					mdgcore_render_block_heading( $heading );
				} ?>

				<?=$block['content']?>

			</div>
		</section>

		<?php

	}}

// SHORTCODE
// ---------

	if ( !function_exists('mdgcore_shortcode_callout') ) {
	function mdgcore_shortcode_callout($atts, $content = null) {

		// CONTENT
	    if ( !$content ) { return; }

		$content = mdgcore_trim_shortcode_content($content);

		// ATTRIBUTES
		extract(shortcode_atts([
			'title' => false,
			'align' => false,
	    ], $atts));

	    // Set up an ad-hoc "block" to render.
	    $block = [
	    	'title'     => $title,
	    	'content'   => $content,
	    	'customize' => [],
	    	'align'     => $align,
	    ];

	    // ALIGNMENT
	    if ( $align ) {
	    	$block['customize'][] = 'align';
	    	$block['align']       = $align;
	    }

		ob_start();

		mdgcore_render_callout( $block );

		$output = ob_get_contents();

	    ob_end_clean();

	    return $output;

	}}
