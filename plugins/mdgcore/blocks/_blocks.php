<?php

// BLOCKS
// ======

// PLUGIN FUNCTION: REGISTER BLOCK
// -------------------------------

	if ( !function_exists('mdgcore_register_block') ) {
	function mdgcore_register_block( $layout_id = '', $render_function_name = '' ) {

		// If no block layout or function is provided, stop.
		if ( empty($layout_id) || empty($render_function_name) ) return false;

		global $mdgcore_render_block_functions;

		// If the global array doesn't exist yet, create it now.
		if ( !isset($mdgcore_render_block_functions) ) $mdgcore_render_block_functions = [];

		$mdgcore_render_block_functions[ $layout_id ] = $render_function_name;

	}}

// TEMPLATE FUNCTION: RENDER BLOCKS
// --------------------------------

	if ( !function_exists('mdgcore_render_blocks') ) {
	function mdgcore_render_blocks ( $source = null ) {

		$source = get_post($source);
		if ( !$source ) return;

		$blocks_data = mdgcore_get_blocks_data( $source );
		if ( empty($blocks_data) ) return;

		foreach ($blocks_data as $block) {

			$block_layout = $block['acf_fc_layout'];

			$render_function = mdgcore_get_block_render_function( $block_layout );

			if ( !$render_function || !function_exists($render_function) ) return;
			
			call_user_func( $render_function, $block );

		}

	}}

// HELPER: GET BLOCKS DATA FROM POST
// ---------------------------------

	if ( !function_exists('mdgcore_get_blocks_data') ) {
	function mdgcore_get_blocks_data( $source, $depth = 0 ) {

		$blocks_data = [];

		// DEPTH LIMIT
		// Safety policy: terminate recursion after 2 bounces to prevent infinite loops.
		if ( $depth > 2 ) return $blocks_data;

		// GET DATA
		// Original method used have_rows() & the_row(). This caused some weird behavior when used in conjunction with layouts that included their own repeater fields. Now we get the raw data array and traverse it explicitly.
		$blocks = get_field( 'post_blocks', $source );

		if ( $blocks ) { foreach ( $blocks as $block ) {

			$block_layout = $block['acf_fc_layout'];

			if ( $block_layout == 'shared' ) {

				$shared_object = $block['block'];

				if ( isset($shared_object) ) {

					// Run this function recursively and flatten the results.
					$shared_blocks_data = mdgcore_get_blocks_data( $shared_object, $depth + 1 );

					$blocks_data = array_merge( $blocks_data, $shared_blocks_data );

				}

			} else {

				$blocks_data[] = $block;

			}

		}}

		return $blocks_data;

	}}

// HELPER: GET BLOCK RENDERER FUNCTION
// -----------------------------------

	if ( !function_exists('mdgcore_get_block_render_function') ) {
	function mdgcore_get_block_render_function( $layout_id = '' ) {

		global $mdgcore_render_block_functions;

		// If no block layout is provided, stop.
		if ( empty($layout_id) ) return false;

		// If the global array doesn't exist yet, create it now.
		if ( !isset($mdgcore_render_block_functions) ) $mdgcore_render_block_functions = [];

		// If no function name has been registered for this block type, stop.
		if ( !isset($mdgcore_render_block_functions[$layout_id]) || empty($mdgcore_render_block_functions[$layout_id]) ) return false;
		
		// Return the function name.
		return $mdgcore_render_block_functions[ $layout_id ];
		
	}}
