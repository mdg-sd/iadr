<?php

// BLOCK: ACTIONS
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_actions_setup', 0 );

	if ( !function_exists('mdgcore_block_actions_setup') ) {
	function mdgcore_block_actions_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('actions', 'mdgcore_render_block_actions');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_register_block') ) {
	function mdgcore_register_block ( $block ) {

		?>

		<section class="entry-actions">
			<div class="entry-block-inner">

				<?php if ( !empty( $block['title'] ) ) {
					$heading = [ 'title' => $block['title'] ];
					mdgcore_render_block_heading( $heading );
				} ?>

				<?php if ( !empty($block['actions']) ) { ?>
				<ul>
				<?php foreach ( $block['actions'] as $action ) {

					$link = $action['link'];

					?>

					<li>
						<a href="<?=$link['url']?>" target="<?=$link['target']?>"><?=$link['title']?></a>
					</li>

				<?php } ?>
				</ul>
				<?php } ?>

			</div>
		</section>

		<?php

	}}
