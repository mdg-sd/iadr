<?php

// BLOCK: SHARED BLOCK GROUP
// =========================

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_shared_setup', 0 );

	if ( !function_exists('mdgcore_block_shared_setup') ) {
	function mdgcore_block_shared_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// CUSTOM POST TYPE
		add_action( 'init', 'mdgcore_regcpt_block' );

		// SHORTCODE
		add_shortcode('block', 'mdgcore_shortcode_block');

		// ADMIN: SHOW "EDIT SHARED BLOCK" BUTTON
		add_action('admin_head', 'mdgcore_admin_show_edit_block');

	}}

// CUSTOM POST TYPE
// ----------------

	if ( !function_exists('mdgcore_regcpt_block') ) {
	function mdgcore_regcpt_block() {

		register_post_type( 'block', [
			'supports'            => [ 'title' ],
			'taxonomies'          => [],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-images-alt2',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'labels'              => [
				'name'                => 'Block Groups',
				'singular_name'       => 'Block Group',
				'menu_name'           => 'Blocks',
				'parent_item_colon'   => 'Parent Block Group:',
				'all_items'           => 'All Block Groups',
				'view_item'           => 'View Block Groups',
				'add_new_item'        => 'Add New Block Group',
				'add_new'             => 'New Block Group',
				'edit_item'           => 'Edit Block Group',
				'update_item'         => 'Update Block Group',
				'search_items'        => 'Search Block Groups',
				'not_found'           => 'No Block Groups Found',
				'not_found_in_trash'  => 'No Block Groups Found in Trash',
			],
		]);

	}}

// SHORTCODE
// ---------

	if ( !function_exists('mdgcore_shortcode_block') ) {
	function mdgcore_shortcode_block($atts, $content = null) {

		extract(shortcode_atts([
			'name' => false,
			'slug' => false,
			'id'   => false,
	    ], $atts));

	    $args = [
            'name'           => $name,
            'posts_per_page' => 1,
            'post_type'      => 'block',
            'post_status'    => 'publish'
    	];

    	if ( !empty($name) ) {
    		$args['name'] = $name;
    	} elseif ( !empty($slug) ) {
    		$args['post_name'] = $slug;
    	} elseif ( !empty($id) ) {
    		$args['ID'] = $id;
    	} else {
    		return;
    	}

	    $blocks = get_posts( $args );

    	if ( empty($blocks) ) return;
    
	    $block_post = $blocks[0];
	    
    	// Use output buffering (ob) to catch echo.

    	ob_start();

        mdgcore_render_blocks($block_post);

        $output = ob_get_contents();

        ob_end_clean();

        return $output;

	}}

// ADMIN: SHOW "EDIT SHARED BLOCK" BUTTON
// --------------------------------------

	if ( !function_exists('mdgcore_admin_show_edit_block') ) {
	function mdgcore_admin_show_edit_block() {

		?><script>

			function mdgcoreAdminShowEditBlock() {

				var adminURL = "<?=admin_url()?>";
				var linkClass = 'mdgcore-block-edit-link';

				var fieldContainers = jQuery('div:not(.clones) > .layout .acf-field-591623125c101');

				// Destroy links created previously.
				jQuery('.'+linkClass).remove();

				jQuery(fieldContainers).each(function( containerIndex, containerElement ) {

					var postID = jQuery(containerElement).find('option[selected]').attr('value');

					var editURL = adminURL + 'post.php?post=' + postID + '&action=edit';

					var editLink = '<p class="'+linkClass+'" style="margin-bottom: 0;"><a href="' + editURL + '" target="_blank">Edit Shared Block</a></p>';

					jQuery(containerElement).append(editLink);

				});

			}

			jQuery(document).ready(function(){
				mdgcoreAdminShowEditBlock();

				/*jQuery('select[id*="field_591623125c101"]').change(function(){
					mdgcoreAdminShowEditBlock();
				});*/
				
			});

		</script><?php

	}}
