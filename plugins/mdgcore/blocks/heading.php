<?php

// BLOCK: HEADING
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_heading_setup', 0 );

	if ( !function_exists('mdgcore_block_heading_setup') ) {
	function mdgcore_block_heading_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('heading', 'mdgcore_render_block_heading');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_heading') ) {
	function mdgcore_render_block_heading ( $block ) {

		$id = sanitize_title($block['title']);

		?>

		<section class="entry-heading">
			<div class="entry-block-inner">

				<h2 id="<?=$id?>"><?=$block['title']?></h2>

				<?php if ( !empty($block['subtitle']) ) { ?>
				<p class="subtitle"><?=$block['subtitle']?></p>
				<?php } ?>

			</div>
		</section>

		<?php

	}}
