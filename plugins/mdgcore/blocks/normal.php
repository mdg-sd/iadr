<?php

// BLOCK: NORMAL (GENERIC WYSIWYG)
// ===============================

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_normal_setup', 0 );

	if ( !function_exists('mdgcore_block_normal_setup') ) {
	function mdgcore_block_normal_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('normal', 'mdgcore_render_block_normal');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_normal') ) {
	function mdgcore_render_block_normal ( $block ) {

		?>

		<section class="entry-block-normal">
			<div class="entry-block-inner">

				<?=$block['content']?>

			</div>
		</section>

		<?php

	}}
