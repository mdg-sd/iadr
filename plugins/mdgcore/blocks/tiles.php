<?php

// BLOCK: TILES
// ============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_tiles_setup', 0 );

	if ( !function_exists('mdgcore_block_tiles_setup') ) {
	function mdgcore_block_tiles_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('tiles', 'mdgcore_render_block_tiles');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_register_block') ) {
	function mdgcore_register_block ( $block ) {

		?>

		<section class="entry-tiles">
			<div class="entry-block-inner">

				<?php if ( !empty( $block['title'] ) ) {
					$heading = [ 'title' => $block['title'] ];
					mdgcore_render_block_heading( $heading );
				} ?>

				<?php if ( !empty($block['tiles']) ) { ?>
				<ul class="entry-tiles-list">
				<?php foreach ($block['tiles'] as $tile ) { ?>

					<li>

						<?php

						// OPTIONAL FIELDS
						$tile_has_fields   = $tile['fields'];
						$tile_has_image    = in_array( 'image',    $tile_has_fields );
						$tile_has_subtitle = in_array( 'subtitle', $tile_has_fields );
						$tile_has_content  = in_array( 'content',  $tile_has_fields );

						// LINK

						$tile_has_link = false;
						$tile_url      = '';
						$tile_target   = '';

						switch ( $tile['type'] ) {

							case 'post':

								$tile_has_link = true;
								$tile_url      = get_permalink( $tile['post'] );
								$tile_target   = '';
								
								// If tile’s title field is empty, use the post title.
								if ( empty($tile['title']) ) {
									$tile['title'] = get_the_title( $tile['post'] );
								}

								break;
							
							case 'url':
								
								$tile_has_link = true;
								$tile_url      = $tile['link']['url'];
								$tile_target   = $tile['link']['target'];

								// If tile’s title field is empty, use the link title field.
								if ( empty($tile['title']) ) {
									$tile['title'] = $tile['link']['title'];
								}

								break;
							
							default: break;
						}

						// IMAGE
						if ( $tile_has_image ) {

							// IMAGE FIELD
							$tile_image     = $tile['image'];
							$tile_image_url = '';

							if ( !empty($tile['image']) ) {
								$tile_image_url = $tile['image']['sizes']['large'];
							} 

							// If no image was set, and the tile is bound to a page or post, attempt to use the featured image.
							if ( empty($tile_image_url) && $tile['type'] == 'post' ) {

								$tile_image_id  = get_post_thumbnail_id( $tile['post']->ID );
								if ( !empty($tile_image_id) ) {
									$tile_image     = wp_get_attachment_image_src( $tile_image_id, 'large' );
									$tile_image_url = $tile_image[0];
								}

							}

						} ?>

						<div class="entry-tile">

							<?php if ( $tile_has_image && !empty($tile_image_url) ) { ?>
							<div class="entry-tile-thumb-container">
								<a href="<?=$tile_url?>" target="<?=$tile_target?>"><div class="entry-tile-thumb" style="background-image: url(<?=$tile_image_url?>);"></div></a>
							</div>
							<?php } ?>

							<div class="entry-tile-caption">

								<div class="entry-tile-title"><a href="<?=$tile_url?>" target="<?=$tile_target?>"><?=$tile['title']?></a></div>

								<?php if ( $tile_has_subtitle && !empty($tile['subtitle']) ) { ?>
									<div class="entry-tile-subtitle"><?=$tile['subtitle']?></div>
								<?php } ?>

								<?php if ( $tile_has_content && !empty($tile['content']) ) { ?>
									<div class="entry-tile-content"><?=$tile['content']?></div>
								<?php } ?>

							</div>

						</div>

					</li>

				<?php } ?>
				</ul>
				<?php } ?>

			</div>
		</section>

		<?php

	}}
