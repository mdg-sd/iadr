<?php

// BLOCK: DIVIDER
// ==============

// SETUP
// -----

	add_action( 'init', 'mdgcore_block_divider_setup', 0 );

	if ( !function_exists('mdgcore_block_divider_setup') ) {
	function mdgcore_block_divider_setup() {

		// OPTION
		$enable_block = get_field('site_enable_block', 'option');
		if ( !$enable_block ) return;

		// RENDERER
		mdgcore_register_block('divider', 'mdgcore_render_block_divider');

		// SHORTCODE
		add_shortcode('divider', 'mdgcore_shortcode_divider');

	}}

// RENDERER
// --------

	if ( !function_exists('mdgcore_render_block_divider') ) {
	function mdgcore_render_block_divider () {

		?>

		<section class="entry-divider">
			<div class="entry-block-inner">
				<hr>
			</div>
		</section>

		<?php

	}}

// SHORTCODE
// ---------

	if ( !function_exists('mdgcore_shortcode_divider') ) {
	function mdgcore_shortcode_divider() {

		ob_start();

		mdgcore_render_block_divider();

		$output = ob_get_contents();

	    ob_end_clean();

	    return $output;

	}}
