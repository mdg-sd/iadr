<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?> class="container">

	<div class="row">

		<!-- IMAGE -->
		<?php if ( has_post_thumbnail() ) { ?>

			<div class="entry-image col-12 mb-4 mt-1 col-sm-3 px-0 mx-2">

				<?php the_post_thumbnail( 'medium' ); ?>

			</div>

		<?php } ?>

		<!-- CONTENT -->
		<div class="entry-content-container col">

			<!-- BODY -->
			<section class="entry-content">
				<div class="entry-content-inner">

					<!-- HEADER -->
					<?php get_template_part( 'header', get_post_type() ); ?>

					<?php the_content(); ?>
				</div>
			</section>

			<!-- BLOCKS -->
			<?php the_blocks(); ?>

		</div>
	</div>

</article>
