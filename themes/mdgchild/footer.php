		</div><!-- .main-container -->

		<footer class="site-footer">

			<div class="footer-inner">

				<section class="site-id">

					<section class="site-image">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?=esc_attr( get_bloginfo( 'name' ))?>" rel="home"><img src="<?=get_stylesheet_directory_uri() . '/images/iadr_logo.png'; ?>" alt="<?=esc_attr( get_bloginfo( 'name' ))?>"></a>
					</section>

					<section class="site-info">
						<?=nl2br( get_option('footer-address')) ?>
					</section>

					<section class="site-copyright">
						<i class="far fa-copyright"></i> <?=date( 'Y' )?> <?=esc_html( get_bloginfo( 'description' ) )?>. All Rights Reserved
					</section>

				</section>

				<section class="footer-social">

					FOLLOW US

					<nav class="social-nav">
						<?php wp_nav_menu( array( 'theme_location' => 'social-menu', 'walker' => new Class_Filters() ) ); ?>
					</nav>

				</section>

			</div>

		</footer>

		<?php wp_footer(); ?>

	</div><!-- .site-container -->
	<!-- promotional video overlay, global -->
	<div class="modal fade" id="vidOverlay" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="vidOverlay" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content container-fluid bg-dark">
				<div class="modal-header pb-0 mb-0 border-bottom-0">
					<h1 class="text-light my-0">2020 Award Winners</h1>
					<button id="vidOverlayClose" type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body row my-0 py-0">
					<div class="video-container col-12">
						<div id="vidFrame">
							<iframe src="https://www.youtube.com/embed/iIiER6rhPpU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>

</html>