<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<?php /* get_template_part('tags/google-tag-manager', 'head'); */ ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<base href="<?=get_bloginfo('url')?>/">

	<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php /* get_template_part('tags/google-tag-manager', 'body'); */ ?>

	<div class="site-container">

		<header class="site-header">

			<section class="site-branding">

				<?php if ( get_header_image() ) { ?>
				<div class="site-image">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?=esc_attr( get_bloginfo( 'name' ))?>" rel="home"><img src="<?php header_image(); ?>" alt="<?=esc_attr( get_bloginfo( 'name' ))?>" class="pb-4 px-1 pb-sm-2 px-sm-0"></a>
				</div>
				<?php }
					$menu_locations		= get_nav_menu_locations();
					$header_links		= wp_get_nav_menu_object( $menu_locations[ 'header-links' ] );
					$header_links_items = wp_get_nav_menu_items( $header_links->term_id, [ 'order' => 'DESC' ] );

				if ( !empty( $header_links_items ) ) : ?>

				<div class="header-links">
					<?php foreach ( $header_links_items as $key => $item ) :
						if ( $key < 3 ) { // limit header links menu to three items ?>

					<a class="header-link" href="<?=$item->url?>"<?php if ( $item->target !== '' ){ ?> target="<?=$item->target?>"<?php } ?>><?=$item->title?></a>
					<?php } endforeach; ?>
				</div>
				<?php endif;?>

			</section>

			<!-- Menu Open Button -->
			<div class="site-menu-button-container">
				<button class="site-menu-button"><i class="fa fa-bars"></i></button>
			</div>

			<div class="site-menu">

				<!-- Menu Close Button -->
				<div class="site-menu-close-button-container">
					<button class="site-menu-close-button"><i class="fa fa-times"></i></button>
				</div>

				<nav class="main-nav">
					<?php wp_nav_menu( [ 'theme_location' => 'main-menu', 'walker' => new Class_Filters() ] ); ?>
				</nav>

			</div>

		</header>

		<div class="main-container">
