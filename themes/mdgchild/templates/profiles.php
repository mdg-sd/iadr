<?php  /* Template Name: Profiles Page */

get_header(); ?>

<main class="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

		get_template_part( 'entry', get_post_type() );

		endwhile; endif;

		$get_featured_profile = new WP_Query( [
			'post_type'			=> 'profile',
			'posts_per_page'	=> 1,
			'order'				=> 'DESC',
			'orderby'			=> 'ID',
			'tax_query'			=> [ [
					'taxonomy'	=> 'profile_category',
					'field'		=> 'slug',
					'terms'		=> 'featured',
				],
			],
		] );

		if ( $get_featured_profile->have_posts() ) :
			while ( $get_featured_profile->have_posts() ) :
				$get_featured_profile->the_post(); ?>

		<section class="entry-divider">
			<div class="entry-block-inner">
				<hr>
			</div>
		</section>
		<?php
				get_template_part( 'entry', get_post_type() );
			endwhile;
		endif;
		wp_reset_postdata();

		$get_profiles = new WP_Query( [
			'post_type' => 'profile',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'orderby' => 'ID'
		] );

		if ( $get_profiles->have_posts() ):
	?>
	<section class="entry-divider">
		<div class="entry-block-inner">
			<hr>
		</div>
	</section>
	<div class="container-fluid mt-5">
		<div class="row">
	<?php while ( $get_profiles->have_posts() ) : $get_profiles->the_post();

		$img = get_the_post_thumbnail_url( $post, 'medium' );
		$bkg = str_replace( '.jpg','_bw.jpg', $img );

		$tax = wp_get_post_terms( $post->ID, 'profile_category');

		if ( !empty( $tax ) ) continue;

		$post_name = $post->post_name; ?>

			<div class="col-6 col-sm-4 col-md-2 profile-grid entry-image mb-4" data-toggle="modal" data-target="#<?=$post_name?>">

				<img src="<?=$img?>" alt="<?=get_display_title()?>">
				<div class="desat" style="background-image:url( <?=$bkg?> );" data-toggle="tooltip" data-html="true" data-placement="top" data-offset="0px" title="<strong><?=get_display_title()?></strong>, <small><?=get_field('qualifications')?><br><?=get_field('tenure')?>"></small></div>

			</div>


			<div class="modal fade" tabindex="-1" id="<?=$post_name?>" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content modal-body d-block p-4 mt-5 container">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>

						<div class="row">

						<!-- IMAGE -->
							<div class="entry-image col-12 mb-4 mt-1 col-sm-3 px-0 mx-2">

								<?php the_post_thumbnail( 'medium' ); ?>

							</div>

							<!-- CONTENT -->
							<div class="entry-content-container col">

								<!-- BODY -->
								<section class="entry-content">
									<div class="entry-content-inner">

										<!-- HEADER -->
										<?php get_template_part( 'header', get_post_type() ); ?>

										<?php the_content(); ?>
									</div>
								</section>

								<!-- BLOCKS -->
								<?php the_blocks(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
	<script type="text/javascript">
		jQuery( function( $ ){
			$( '[data-toggle="tooltip"]' ).tooltip( { delay: { 'show': 300, 'hide': 50 } } );

			$( function(){
				if( window.location.hash ) {
					var hash = window.location.hash;
					$( hash ).modal( 'toggle' );
				}
			});
			$( document ).on( 'click', 'div.profile-grid', function( e ) {

				var id = $( this ).attr( 'data-target' );

				if ( $( id ).length > 0 ) {

					e.preventDefault();

					if ( window.history && window.history.pushState ) {
						// URL UPDATE
						history.pushState( '', document.title, window.location.href.replace(/#.*$/,'') + id );
					}
				}

			});
		});
	</script>
	<?php endif; ?>
</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>