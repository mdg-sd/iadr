<?php /* Template Name: Blank Page Template */ ?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<?php // get_template_part('tags/google-tag-manager', 'head'); ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<base href="<?=get_bloginfo('url')?>/">

	<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php // get_template_part('tags/google-tag-manager', 'body'); ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<!-- CONTENT -->

			<?php the_content(); ?>

		<?php endwhile; endif; ?>

	<?php // wp_footer(); ?>

</body>

</html>

