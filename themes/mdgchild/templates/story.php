<?php  /* Template Name: Share Your Story Page */

get_header(); ?>

<main class="main">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<!-- ENTRY -->
	<?php get_template_part( 'entry', get_post_type() ); ?>

<?php endwhile; endif; ?>

	<?php if ( have_rows( 'stories' ) ) { ?>

		<ul class="story-share container">

		<?php $i = 0;
			while ( have_rows( 'stories' ) ) : the_row();
				$story			= get_sub_field( 'story' );
				$is_featured	= $i < 3;
				$not_featured	= $i > 2;
				$is_even		= $i % 2 === 0;
				$is_odd			= $i % 2 !== 0;
				// link vars
				$has_link		= false;
				$link_url		=
				$link_title		=
				$link_target	= '';
				if ( !empty( $story[ 'link' ] ) ) {
					$has_link		= true;
					$link_url		= $story[ 'link' ][ 'url' ];
					$link_title		= $story[ 'link' ][ 'title' ];
					$link_target	= '_blank';
				}
			?>

			<?php if ( $is_featured ) { ?>
			<li class="row mb-4 grey rounded <?php if ( $is_even ) { echo 'flex-row-reverse text-right'; } ?>">
			<?php } else if ( $not_featured && $is_odd ) { ?>
			<li class="row mb-3">
			<?php } ?>
			<?php if ( $not_featured ) { ?><div class="col-6"><div class="row grey rounded <?php if ( $is_odd ) { echo 'mr-0'; } ?>"><?php } ?>
				<!-- IMAGE -->
				<?php if ( $story[ 'type' ] === 'image' ) {

					// vars
					$img		= $story[ 'image' ];
					$url		= $img[ 'url' ];
					$thumb		= $is_featured ? $img[ 'sizes' ][ 'medium' ] : $img[ 'sizes' ][ 'large' ];

					?>
				<div class="<?php if ( $is_featured ) { echo 'col-sm-4'; } else { echo 'col-sm-12 d-flex justify-content-center'; } ?> py-3">
					<a href="<?php if ( $has_link ) { ?><?=$link_url;?><?php } else { ?><?=$url?><?php } ?>" title="<?=$story[ 'name' ]?>" class="story-pic">
						<img src="<?=$thumb?>" alt="<?php if ( $has_link ) { ?><?=$link_title;?><?php } else { ?><?=$story[ 'name' ]?><?php } ?>" />
					</a>
				</div>
				<?php } ?>

				<!-- VIDEO -->
				<?php if ( $story[ 'type' ] === 'video' ) { ?>
				<div class="col<?php if ( $not_featured ) { ?>-12<?php }?>">
					<div class="video-container my-3">
						<div>
							<?=$story[ 'video' ]?>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php if ( !empty( $story[ 'pullquote' ] ) ) { ?>
				<div class="col">
					<blockquote class="blockquote">
						<?php if ( $i > 2 ) { ?><small><?php }?>
						<p>&ldquo;<?=$story[ 'pullquote' ]?>&rdquo;</p>
						<?php if ( !empty( $story[ 'name' ] || $has_link ) ) { ?>
						<footer class="blockquote-footer"><?php if ( $has_link ) { ?><a title="<?=$link_title;?>" href="<?=$link_url;?>" target="<?=$link_target;?>"><?php } ?><cite title="<?php if ( $has_link ) { ?><?=$story[ 'link' ][ 'title' ]?><?php } else { ?><?=$story[ 'name' ]?><?php } ?>"><?=$story[ 'name' ]?></cite><?php if ( $has_link ) { ?> | READ MORE &raquo;</a><?php } ?></footer><?php }?>
						<?php if ( $i > 2 ) { ?></small><?php }?>
					</blockquote>
				</div>
				<?php } ?>
				<?php if ( $not_featured ) { ?></div></div><?php } ?>

			<?php if ( ( $not_featured ) && ( $is_even ) ) { ?></li><?php } ?>

			<?php $i++; endwhile; ?>

		</ul>

	<?php } ?>

</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>