<?php /* Template Name: Home */

	get_header();

	get_template_part( 'homepage/hero' );

?>

<main class="page-template-homepage">

	<section class="main">

		<?php if ( get_field( 'timeline_enabled' ) ) { ?>

		<?=get_template_part( 'homepage/century' ); ?>

		<?php }

		the_blocks();

		if ( get_field( 'countdown_enabled' ) ) { ?>

		<?=get_template_part( 'homepage/countdown' ); ?>

		<?php } else { ?><hr><?php } ?>
		<section class="social-feed">
			<?=do_shortcode( '[ff id="1"]' ); ?>
		</section>
		<?php if ( get_field( 'slider_enabled' ) ) { ?>
		<hr>
		<?php
			the_blocks( get_field( 'slider_block' ) );
		 } ?>

	</section>

</main>

<?php get_footer(); ?>