<?php /* Template Name: Timeline Template */ ?>
<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<?php // get_template_part('tags/google-tag-manager', 'head'); ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<?php
		// SCRIPTS/CSS
		wp_enqueue_script ( 'greensock', get_stylesheet_directory_uri() . '/js/lib/greensock/TweenMax.min.js', [], false );
		wp_enqueue_script ( 'greensock-css', get_stylesheet_directory_uri() . '/js/lib/greensock/plugins/CSSPlugin.min.js', ['greensock'], false );
		wp_enqueue_script ( 'greensock-scrollto', get_stylesheet_directory_uri() . '/js/lib/greensock/plugins/ScrollToPlugin.min.js', ['greensock'], false );

		wp_enqueue_script ( 'waypoints-jquery', '//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js', ['jquery'], '', false );
		wp_enqueue_script ( 'waypoints-sticky', '//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js', ['waypoints-jquery'], '', false );
		wp_enqueue_script ( 'waypoints-inview', '//cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/inview.min.js', ['waypoints-jquery'], '', false );

		// aos scroll library
		wp_enqueue_style ( 'aos_css', get_stylesheet_directory_uri() . '/js/lib/node_modules/aos/dist/aos.css' );
		wp_enqueue_script ( 'aos_js', get_stylesheet_directory_uri() . '/js/lib/node_modules/aos/dist/aos.js', [], '', false );

		// custom scrollbars
		wp_enqueue_style  ( 'custom_scrollbars_css','//cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css' );
		wp_enqueue_script ( 'custom_scrollbars', '//cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js', ['jquery'], '', true );

		wp_head();
		wp_enqueue_script ( 'timeline', get_stylesheet_directory_uri() . '/js/timeline.js', ['portal'], '20210304', true );
	?>

</head>

<body <?php body_class(); ?>>

<?php
	/* get_template_part('tags/google-tag-manager', 'body'); */
	$decades 			= get_terms( [ 'taxonomy' => 'timeline_decade', 'hide_empty' => true ] );
	$years_container 	= [];
?>

	<div class="timeline-container">

		<!-- Menu Open Button -->
		<div class="site-menu-button-container">
			<button class="site-menu-button"><i class="fa fa-bars"></i></button>
		</div>

		<div class="timeline-content">

			<div id="timeline-home" class="timeline-home">
				<a class="home-btn" href="<?=get_home_url()?>"><i class="fal fa-angle-double-left"></i><i class="fal fa-home"></i></a>
				<div class="container-fluid">
					<div class="timeline-splash">
						<img src="<?=get_stylesheet_directory_uri() . '/images/site_id_timeline.png'; ?>" alt="Celebrating a Century of Discovery">
						<h1><strong>CELEBRATING</strong> A CENTURY OF <strong>DISCOVERY</strong></h1>
						<p>Travel through 100 years of innovation, progress and pioneering research.</p>
						<a href="#decade-<?=$decades[0]->slug?>" rel="decade" class="continue"><i class="fal fa-angle-double-down"></i></a>
					</div>
				</div>

			</div>

			<div class="timeline-cascade">
				<div class="menu" id="decade-nav">

					<!-- Menu Close Button -->
					<div class="site-menu-close-button-container">
						<button class="site-menu-close-button"><i class="fa fa-times"></i></button>
					</div>

					<nav class="timeline-menu">

						<ul class="nav">
							<li class="icon"><a href="#timeline-home" rel="decade"><img src="<?=get_stylesheet_directory_uri() . '/images/timeline_icon.png'; ?>" alt="Back to top"></a></li>
							<?php
								if ( count( $decades ) > 0 ) {

									foreach( $decades as $key => $decade ) { ?>
										<li>
											<a href="#decade-<?=$decade->slug?>" rel="decade"><?=$decade->name?><div id="link-<?=$decade->slug?>" class="indicator" data-aos="nav-indicator" data-aos-anchor="#decade-placement-<?=$decade->slug?>" data-aos-once="false" data-aos-mirror="true" data-aos-offset="500" data-aos-anchor-placement="top-bottom"></div></a>
										</li>
									<?php
									}
								}
							?>
							<li class="home"><a href="<?=get_home_url()?>"><i class="fal fa-angle-double-left"></i> HOME</a></li>
						</ul>

					</nav>

				</div>
				<?php foreach ( $decades as $key => $decade ) { ?>

				<div id="decade-<?=$decade->slug?>" class="timeline-decade">

					<div class="decade-placement" id="decade-placement-<?=$decade->slug?>"></div>

					<div class="timeline-events">

						<div class="container-fluid">

							<ul class="timeline-list">
								<li class="divider-line" data-aos="expand-top" data-aos-anchor="#decade-placement-<?=$decade->slug?>" data-aos-duration="350" data-aos-easing="ease-in-out-quart"></li>

								<li class="timeline-header decade-num">

									<?=$decade->name?><small>s</small>

								</li>

								<li class="timeline-header decade-about">

									<h3><?=get_field( 'title', $decade )?></h3>
									<?=get_field( 'about', $decade )?>

								</li>



							</ul>

						</div>

					</div>
					<?php // disabling the decade backgrounds for now
						/*
						<div class="decade-background" style="background-image: url(<?=get_field( 'background_image', $decade )?>);" data-aos="fade" data-aos-duration="400" data-aos-easing="ease-in-out-quad" data-aos-anchor="#decade-placement-<?=$decade->slug?>" data-aos-once="false" data-aos-mirror="false" data-aos-offset="300" data-aos-anchor-placement="top-bottom"></div>
						*/?>

				</div>

				<?php } ?>

			</div>

		</div>

		<?php wp_footer(); ?>

	</div>

</body>

</html>