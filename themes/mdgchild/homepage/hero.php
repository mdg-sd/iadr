<?php

	wp_enqueue_script ( 'greensock', 		get_stylesheet_directory_uri() . '/js/lib/greensock/TweenMax.min.js' );
	wp_enqueue_script ( 'timelinelite', 	get_stylesheet_directory_uri() . '/js/lib/greensock/TimelineLite.min.js' );
	wp_enqueue_script ( 'splittext', 		get_stylesheet_directory_uri() . '/js/lib/greensock/utils/SplitText.min.js' );

	$labels = get_field( 'hero_labels' );
	$pause 	= get_field( 'hero_settings' )['pause_length'];

	if ( get_field( 'hero_settings' )['randomize'] ) shuffle( $labels );

	if ( !get_field( 'hero_settings' )['plays_once'] ) {
		$loops = get_field('hero_settings')['loops'];
		$label_repeats = [];
		for ( $i = 0; $i <= $loops; $i++ ) {
			$label_repeats = array_merge( $labels, $label_repeats );
		}
		$labels = $label_repeats;
	}

	$first_label = $labels[0]['label'];
?>
<section class="hero-banner<?php if ( !get_field( 'timeline_enabled' ) ) { ?> no-timeline<?php } ?>">

	<div class="hero-text">

		<div class="hero-text-top">Celebrating a century of</div>
		<div class="hero-text-mid"><?=$first_label?></div>
		<div class="hero-text-btm">#IADR100</div>

	</div>

</section>

<script type="text/javascript">

	var heroController = function(selector) {

		selector = selector || '.hero-banner';

		this.playing = false;

		this.textbox = jQuery( selector + ' .hero-text-mid' );

		this.lines = [ <?php foreach ( $labels as $key => $label ) :?>
				'<?=$label[ 'label' ] ?>',
			<?php endforeach; ?>
		];

		this.currentLine = 0;

		<?php if ( $loops === '0' ) { ?>

			this.infinite = true;

		<?php } else { ?>

			this.infinite = false;

		<?php } ?>

		// Sequence

		this.begin = function() {

			var control = this;

			if ( this.playing ) { return; }

			this.playing = true;

			control.advance();

		}

		this.advance = function() {

			var control = this;

			var nextLine = this.getNextLine();

			if ( nextLine === 0 ) {

				if ( !this.infinite ) {

					control.end();
					return;

				} else {

					this.currentLine = -1;

				}

			}

			this.toggleText();

		}

		this.toggleText = function() {

			var tl 			= new TimelineLite,
			control 		= this,
			currentWord 	= this.textbox,
			previousWord 	= new SplitText( currentWord, { type: 'words, chars' }),
			prevChars 		= previousWord.chars;

			TweenLite.set( currentWord, { perspective:800 });

			tl.staggerTo( prevChars, .45, { opacity:0, y:10, ease:Power3.easeInOut }, .05 );
			tl.call( swapText, [], this, '-=0.1' );

			function swapText() {
				previousWord.revert();
				this.updateText();
				nextWord 	= new SplitText( currentWord, { type: 'words, chars' });
				nextChars 	= nextWord.chars;
				tl.staggerFrom( nextChars, 0.8, { opacity:0, scale:0, y:80, rotationX:180, transformOrigin:'0% 50% -50', ease:Power3.easeOut}, 0.01, '+=0');
				tl.call( callNext, [], this, '+=<?=$pause?>' );
			}

			function callNext() {

				this.advance();

			}

		}

		this.end = function() {

			var control = this;

			this.playing = false;

		}

		// Mechanics

		this.getNextLine = function() {

			return ( this.currentLine + 1 ) % this.lines.length;

		}

		this.updateText = function() {

			var nextLine = this.getNextLine();

			var lineHTML = this.lines[ nextLine ];

			this.textbox.html( lineHTML );

			this.currentLine = nextLine;

		}

	};

	jQuery( 'document' ).ready( function(){

		hero = new heroController('.hero-banner');

		setTimeout( function() {
			hero.begin();
		}, <?=$pause*1000?>);

	});

</script>