<script type="text/javascript">

	jQuery( document ).ready( function( $ ) {

		var dateSet 	= '<?=get_field('end_date')?>',
			dateUpdate	= dateSet.replace('00:03:00','08:00:00'),
			futureDate	= new moment( dateUpdate, 'DD-MM-YYYY HH:mm:ss' ),
			$day		= $('.days'),
			$hr			= $('.hours'),
			$min		= $('.minutes');

		function padNum( num ) {
			return ( num < 10 ? '0' : '' ) + num;
		}

		( function updateCountdown() {

			var now			= new moment(),
				til			= moment.duration( futureDate.diff( now ) ),
				days		= padNum( parseInt( til.asDays() ) ),
				hrs			= padNum( til.hours() ),
				mins		= padNum( til.minutes() );

			$day.text( days );
			$hr.text( hrs );
			$min.text( mins );

			setTimeout( updateCountdown, 60000 );

		})();

	});

</script>
<hr>
<section class="countdown">
	<div class="label"><?=get_field( 'counter_title' )?></div>
	<div class="circles">
		<div class="num days"></div>
		<div class="num hours"></div>
		<div class="num minutes"></div>
	</div>
</section>
<hr>