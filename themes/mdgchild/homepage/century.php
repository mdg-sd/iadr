<div class="century-callout">
		<a class="explore" href="/timeline/"><img class="century-icon" src="<?=get_stylesheet_directory_uri() . '/images/timeline_icon.png'; ?>" alt="100 Years"></a>
		<div class="explore-box">
			<p>Explore 100 years of inspiring IADR history and the groundbreaking research that will shape the next century.</p>
			<a class="explore" href="/timeline/"><i class="fas fa-angle-double-right"></i> Start Exploring the Centennial Timeline</a>
		</div>
	</div>