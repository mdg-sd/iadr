<?php

// GENERAL SETTINGS - SITE SPECIFIC
// --------------------------------

	add_filter('admin_init', 'site_specific_general_settings');

	function site_specific_general_settings() {

		// FOOTER ADDRESS
		register_setting( 'general', 'footer-address', 'esc_attr' );
		add_settings_field( 'footer-address', '<label for="footer-address">'.__('Company Address<br><small>(appears in footer)</small>' , 'footer-address' ).'</label>' , 'site_specific_general_settings_html', 'general');
	}

	function site_specific_general_settings_html() {
		$value = get_option( 'footer-address', '' );
		echo '<textarea type="text" id="footer-address" name="footer-address" rows="4" cols="37">' . $value . '</textarea>';
	}

// EXTERNAL FONTS / SCRIPTS
// -----------------------

	add_action( 'wp_enqueue_scripts', 'mdgchild_load_external' );

	function mdgchild_load_external() {

		# Enqueue fonts here.
		wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=PT+Sans:400,700|Montserrat:100,400,600,700&display=swap', array() );

		# Enqueue scripts here.

	}

// MENUS
// -----

	add_action( 'after_setup_theme', 'mdgchild_register_menus' );

	function mdgchild_register_menus() {

		register_nav_menus([

			// MAIN MENU
			'main-menu' => 'Main Menu',
			// SOCIAL MENU - FOOTER
			'social-menu' => 'Social Menu',
			// HEADER LINKS
			'header-links' => 'Header Links',

		]);

	}

// SIDEBARS
// --------

	// add_action( 'widgets_init', 'mdgchild_register_widgets' );

	function mdgchild_register_widgets() {

		// MAIN SIDEBAR
		register_sidebar([
			'name'          => 'Main Sidebar',
			'id'            => 'sidebar-1',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => "</section>",
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		]);

		# Add sidebars (widget areas) here.

	}

// IMAGE SIZES
// -----------

	// add_image_size( 'slug', 9999, 9999, true );
	# Add image sizes here.

// OPTION: HEADER IMAGE
// --------------------

	# Uncomment to enable.
	add_action( 'init', 'mdgchild_register_custom_header' );

	function mdgchild_register_custom_header() {

		$custom_header_args = [
			'height'        => 112,
			'width'         => 205,
			'flex-width'    => true,
			'flex-height'   => true,
			'default-image' => get_stylesheet_directory_uri() . '/images/site_logo.png',
			'uploads'       => false,
		];

		add_theme_support( 'custom-header', $custom_header_args );

	}

// OPTION: READ MORE TEXT
// ----------------------

	add_action( 'customize_register', 'mdgchild_customize_readmore' );

	function mdgchild_customize_readmore( $wp_customize ) {

		$wp_customize->add_section( 'mdgchild_readmore' , [
		    'title' => 'Read More Text',
		]);

		$wp_customize->add_setting( 'mdgchild_readmore' , [
		    'default'     => '&amp;hellip; <a href="%s">(more)</a>',
		    'transport'   => 'refresh',
		]);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'mdgchild_readmore', [
			'label'    => '“Read More” Text',
			'section'  => 'mdgchild_readmore',
			'settings' => 'mdgchild_readmore',
			'type'     => 'text',
		]));

	}

	add_filter( 'excerpt_more', 'mdgchild_excerpt_more' );

	function mdgchild_excerpt_more( $more ) {

		$permalink = get_the_permalink();

		$default = '&hellip; <a href="%s">(more)</a>';
		$format  = get_theme_mod('footer_copyright_text', $default);

		return sprintf( $format, $permalink );

	}


	function custom_wpautop( $content ) {
		if ( get_field( 'wpautop', get_the_ID() ) == 'false' ) {
			return $content;
		} else {
			return wpautop( $content );
		}
	}

	remove_filter('the_content', 'wpautop');
	add_filter('the_content', 'custom_wpautop');