<header class="entry-header">

	<!-- TITLE -->
	<?php if ( !hide_title() ) { ?>
	<h1 class="entry-title"><?=get_display_title()?><?php if ( get_field('qualifications') ) { ?><small><strong>, <?=get_field('qualifications')?></strong></small><?php } ?></h1>
	<?php } ?>

	<!-- META -->
	<h2 class="my-1">The <?=get_field('sequence')?> President of the IADR</h2>
	<h3 class="my-1"><?=get_field('tenure')?></h3>
	<h4 class="my-1"><?=get_field('other')?></h4>

</header>
