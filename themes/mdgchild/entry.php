<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>

	<!-- IMAGE -->
	<?php get_template_part('content', 'entry-image'); ?>

	<!-- CONTENT -->
	<div class="entry-content-container">

		<!-- HEADER -->
		<header class="entry-header">

			<!-- TITLE -->
			<?php if ( !hide_title() ) { ?>
			<h1 class="entry-title"><?=get_display_title()?></h1>
			<?php } ?>

			<!-- META -->
			<?php if ( !hide_title() && !empty(get_subtitle()) ) { ?>
			<section class="entry-meta"><?=get_subtitle()?></section>
			<?php } ?>

		</header>

		<!-- BODY -->
		<section class="entry-content">
			<div class="entry-content-inner">
				<?php the_content(); ?>
			</div>
		</section>

		<!-- BLOCKS -->
		<?php the_blocks(); ?>

	</div>

	<!-- PAGINATION -->
	<?php get_template_part( 'nav', 'entry-content' ); ?>

</article>
