'use strict';

if ( navigator.userAgent.match( /Trident\/7\./) ) { // if IE
	$( 'body' ).on( 'mousewheel', function () {
		// remove default behavior
		event.preventDefault();

		//scroll without smoothing
		var wheelDelta 				= event.wheelDelta;
		var currentScrollPosition 	= window.pageYOffset;
		window.scrollTo( 0, currentScrollPosition - wheelDelta );
	});
}

var _decadesRequested = [];

function _ajaxRequestEvents( decadeID ){

	if ( jQuery.inArray( decadeID, _decadesRequested ) === -1 ) {

		_decadesRequested.push( decadeID );

		var $decade 	= $( '#decade-' + decadeID );
		$decade.addClass( 'loading' );

		var	call 		= new PortalCall({
				method: 'namespace/v1/events',
				data: {
					'decade': decadeID,
				},
				callback: function( output ) {

					$decade.find( 'ul.timeline-list' ).append( output.html );

					$decade.find( '.decade-placement' ).css({ 'height': $decade.outerHeight() + 300 });

					var prevAlternate		= false,
						prevHeight			= 0,
						yearsAdded			= $decade.find( '.timeline-year' ).toArray();

					$decade.find( '.year-panel' ).each( function() {

						var $panel 			= $( this ),
							offset 			= $panel.offset(),
							yPos			= Math.round( offset.top ) + prevHeight + 'px',
							panelHeight		= $panel.outerHeight(),
							$panelTrigger 	= $panel.find( '.trigger' ),
							$img			= $panel.find( '.entry-image > img' ),
							$etc 			= $panel.find( '.year-etc' ),
							$etcClose		= $panel.find( '.etc-close' ),
							$idString		= $panel.parent().attr( 'id' ),
							isAlternate		= $panel.parent().hasClass( 'alternate' ),
							$parent			= '#' + $idString,
							$id 			= $idString.replace( 'year-', 'year-panel-' ),
							listenTarget	= $idString.replace( 'year-', 'aos:in:panel-animate-' ),
							calcHeight		= $img.length ? panelHeight + $img.data( 'img-height' ) : panelHeight;

						if ( prevAlternate ) {
							yPos = parseInt( yPos ) + prevHeight + parseInt( $panel.parent().css( 'margin-top' ) ) + 'px';
						}

						var	$placeholder = $( '<div class="year-placement" style="top:' + yPos + ';height:' + calcHeight + 'px;" id="' + $id + '"></div>' );

						$img.attr( 'draggable', false )
							.attr( 'ondragstart', 'return false;' );

						$etc.css( { 'max-height': 0 } )
							.mCustomScrollbar( {
								theme: 'light-thin'
							} );

						$( 'body' ).append( $placeholder );

						if ( isAlternate ) {
							prevAlternate = true;
						}
						prevHeight = panelHeight;

						$panelTrigger.on( 'click', function( e ){

							TweenMax.to( window, 0.35, { scrollTo: { y: $parent, autoKill: true, offsetY: 200 }, ease: Power2.easeInOut });

							$etc.css({ 'max-height': $etc.outerHeight() });

							function toggleActive() { $panel.toggleClass( 'active' ); }

							if ( $etc.height() > 5 ) {
								TweenMax.to( $etc, 0.5, { delay: .25, maxHeight: 0, autoAlpha: 0, ease: Power2.easeOut, onStart: toggleActive });
								TweenMax.to( $etcClose, 0.5, { delay: .25, rotation: '+=180', autoAlpha: 0, ease: Power2.easeOut });
							} else {
								TweenMax.to( $etc, 0.75, { maxHeight: $panel.outerHeight(), autoAlpha: 1, ease: Power2.easeOut });
								TweenMax.to( $etcClose, 0.75, { rotation: '+=180', autoAlpha: 1, ease: Power2.easeOut, onComplete: toggleActive });
							}
						});

						// REPOSITION NEXT ELEMENT TOP OFFSET
						var $next 	= $panel.parent().next().find( '.year-panel' );
						if ( $next.length > 0 ) { 														// if next
							$( document ).on( listenTarget, function( e ){ 								// listen for anim init
								yearsAdded.forEach( function( year, i ){								// update Y positions
									var triggerID 		= $( year ).attr( 'id' ).replace( 'year-', 'year-panel-' ),
										$trigger 		= $( '#' + triggerID ),
										offsetUpdate	= $( year ).find( '.year-panel' ).offset(),
										// panelMargin		= parseInt( $( year ).css( 'margin-top' ) ), // seems not to help??
										yPosNew			= ( Math.round( offsetUpdate.top ) - 80 ) + 'px';
									$trigger.css( { 'top': yPosNew } ); //, 'left': i + '0px' }); 		// for easier debug visual
								});
								$( document ).off( listenTarget );										// remove listener
								Waypoint.refreshAll();													// refresh scroll libraries
								AOS.refresh();
							});
						}

					});
					Waypoint.refreshAll();
					AOS.refresh();
					$decade.removeClass( 'loading' );
				}
			}).send();

	} else { return; }

}

var _initialize = {

	init: function() {

		_initialize._baseScrollInit();
		_initialize._magnetNavYears();
		_initialize._timelineEnable();
		_initialize._mobileMenu();

	},

	_baseScrollInit: function(){

		// DOCUMENT NAV-SCROLL SETTINGS
		$( document ).on( 'click', 'a[href^=\\#]', function( e ) {

			var id = $( this ).attr( 'href' );

			if ( $( id ).length > 0 ) {

				e.preventDefault();
				TweenMax.to( window, 0.35, { scrollTo: { y: id, autoKill: true, offsetY: 50 }, ease: Power2.easeInOut });
				if ( window.history && window.history.pushState ) {
					// URL UPDATE
					history.pushState( '', document.title, window.location.href.replace(/#.*$/,'') + id );
				}
			}

		});

		// DECADES SCROLL SETTINGS
		$( '.timeline-decade' ).each( function(){
			var $decade 		= $( this ),
				$decadeID		= $decade.attr( 'id' ).replace( 'decade-', '' ),
				viewingDecade 	= false;

			function viewCheck() {
				// make sure we're not skipping down the page before ajax
				if ( viewingDecade ) {
					_ajaxRequestEvents( $decadeID );
				}
			}

			var ajaxTrigger = new Waypoint.Inview({
				element: $decade[0],
				enter: function( direction ) {
					viewingDecade = true;
					setTimeout( viewCheck, 200 );
				},
				exited: function( direction ) {
					viewingDecade = false;
				}
			});

		});

	},

	_magnetNavYears: function() {

		var $nav	= $( '.timeline-menu' );

		var sticky 	= new Waypoint.Sticky({
			element: $nav[0]
		});
	},

	_timelineEnable: function() {
		AOS.init( {
			once: true,
			offset: 0,
			duration: 600,
			easing: 'ease-out-quart'
		} );
	},

	_mobileMenu: function() {
		// mobile menu
		$( '.site-menu-button' ).click( function () {
			$( '#decade-nav' ).toggleClass( 'open' );
			$( 'body' ).toggleClass( 'fixedPosition' );
		});

		$( '.site-menu-close-button' ).click( function () {
			$( '#decade-nav' ).toggleClass( 'open' );
			$( 'body' ).toggleClass( 'fixedPosition' );
		});
	}

};

$( document ).ready( _initialize.init );