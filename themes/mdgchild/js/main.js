var $ = jQuery.noConflict();

$(document).ready(function() {

	// mobile menu
	$( '.site-menu-button' ).click( function () {
		$( '.site-menu' ).toggleClass( 'open' );
		$( 'body' ).toggleClass( 'fixedPosition' );
	});

	$( '.site-menu-close-button' ).click( function () {
		$( '.site-menu' ).toggleClass( 'open' );
		$( 'body' ).toggleClass( 'fixedPosition' );
	});

	// accordion submenu
	$( '#menu-main-menu li.has-sub>a' ).on( 'click', function (e) {

		if ( $( window ).width() < 768 ) {

			var element = $( this ).parent( 'li' );

			if ( element.hasClass( 'open' ) ) {

				element.removeClass( 'open' );
				element.find( 'li' ).removeClass( 'open' );
				element.find( 'ul' ).slideUp();

			} else {
				e.preventDefault();
				element.addClass( 'open' );
				element.children( 'ul' ).slideDown();
				element.siblings( 'li' ).children( 'ul' ).slideUp();
				element.siblings( 'li' ).removeClass( 'open' );
				element.siblings( 'li' ).find( 'li' ).removeClass( 'open' );
				element.siblings( 'li' ).find( 'ul' ).slideUp();

			}
		}
	});

	// toolkit specific functions
	function copyToClipboard( target ) {
		const el = document.createElement( 'textarea' );
		el.value = target.textContent;
		el.setAttribute( 'readonly', '' );
		el.style.position = 'absolute';
		el.style.left = '-9999px';
		document.body.appendChild( el );
		const selected =
			document.getSelection().rangeCount > 0
			? document.getSelection().getRangeAt( 0 )
			: false;
		el.select();
		document.execCommand( 'copy' );
		document.body.removeChild( el );
		target.classList.add( 'copied' );
		setTimeout( resetPseudo, 1500, target );
		if ( selected ) {
			document.getSelection().removeAllRanges();
			document.getSelection().addRange( selected );
		}
	};

	function resetPseudo( target ) {
		target.classList.remove( 'copied' );
	}

	if ( $( '#toolkit' ).length ) {

		var btns = document.querySelectorAll( 'button.list-group-item' );
		for (var i = 0; i < btns.length; i++) {
			btns[i].addEventListener( 'click', function( e ) {
				copyToClipboard( e.target );
			});
		}
	}

	if ( $( '.vid-overlay-opener' ).length ) {

		// open modal
		$( '.vid-overlay-opener' ).click( function() {
			$('#vidOverlay').modal('show');
		});
		// refresh youtube on close to stop playing in background
		$('#vidOverlayClose').click( function() {
			$src = $( '#vidFrame > iframe' ).attr( 'src' );
        	$( '#vidFrame > iframe' ).attr( 'src', $src );
    	});

	}

}) // on ready