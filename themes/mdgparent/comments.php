<?php if ( 'comments.php' == basename( $_SERVER['SCRIPT_FILENAME'] ) ) return; ?>

<section class="comments">

	<?php if ( have_comments() ) : 
		global $comments_by_type;
		$comments_by_type = separate_comments( $comments );

		if ( ! empty( $comments_by_type['comment'] ) ) : ?>
			<section class="comments-list">

				<h3 class="comments-title"><?php comments_number(); ?></h3>

				<?php if ( get_comment_pages_count() > 1 ) : ?>
				<nav class="comments-nav-above comments-navigation">
					<?php paginate_comments_links(); ?>
				</nav>
				<?php endif; ?>

				<ul>
					<?php wp_list_comments( 'type=comment' ); ?>
				</ul>

				<?php if ( get_comment_pages_count() > 1 ) : ?>
				<nav class="comments-nav-below comments-navigation" role="navigation">
					<?php paginate_comments_links(); ?>
				</nav>
				<?php endif; ?>

			</section>
		<?php endif; ?>

		<?php if ( ! empty( $comments_by_type['pings'] ) ) : 
			$ping_count = count( $comments_by_type['pings'] ); ?>

			<section class="trackbacks-list">

				<h3 class="comments-title ping-count"><?=$ping_count?> <?=( $ping_count > 1 ? 'Trackbacks' : 'Trackback' )?></h3>

				<ul>
					<?php wp_list_comments( 'type=pings&callback=mdgparent_pings' ); ?>
				</ul>

			</section>

		<?php endif; ?>

	<?php endif; ?>

	<!-- COMMENT FORM -->
	<?php if ( comments_open() ) comment_form(); ?>

</section>