		</div><!-- .main-container -->

		<footer class="site-footer">

			<section class="site-copyright">
				&copy; <?=date( 'Y' )?> <?=esc_html( get_bloginfo( 'name' ) )?>. All rights reserved.
			</section>

		</footer>

		<?php wp_footer(); ?>

	</div><!-- .site-container -->

</body>

</html>