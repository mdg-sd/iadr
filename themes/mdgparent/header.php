<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<?php /* get_template_part('tags/google-tag-manager', 'head'); */ ?>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />

	<base href="<?=get_bloginfo('url')?>/">

	<title><?php wp_title( ' | ', true, 'right' ); ?></title>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php /* get_template_part('tags/google-tag-manager', 'body'); */ ?>

	<div class="site-container">

		<header class="site-header">

			<section class="site-branding">

				<?php if ( get_header_image() ) { ?>
				<div class="site-image">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?=esc_attr( get_bloginfo( 'name' ))?>" rel="home"><img src="<?php header_image(); ?>" alt="<?=esc_attr( get_bloginfo( 'name' ))?>"></a>
				</div>
				<?php } ?>

				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?=esc_attr( get_bloginfo( 'name' ))?>" rel="home"><?=esc_html( get_bloginfo( 'name' ) )?></a>
				</h1>

				<div class="site-description">
					<?php bloginfo( 'description' ); ?>
				</div>

			</section>

			<nav class="site-menu">
				<?php wp_nav_menu( [ 'theme_location' => 'main-menu' ] ); ?>
			</nav>

			<section class="site-search">
				<?php get_search_form(); ?>
			</section>

		</header>

		<div class="main-container">
