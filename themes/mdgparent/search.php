<?php get_header(); ?>

<main class="main">

	<header class="main-header">

		<h1 class="main-title">
			<span class="main-type">Search:</span>
			<?=get_search_query()?>
		</h1>

	</header>

	<?php if ( have_posts() ) : ?>

		<section class="archive">
		<?php while ( have_posts() ) : the_post(); ?>

			<!-- ENTRY -->
			<?php get_template_part( 'entry', get_post_type() ); ?>

		<?php endwhile; ?>
		</section>

		<!-- PAGINATION -->
		<?php get_template_part( 'nav', 'archive' ); ?>

	<?php else : ?>

		<section class="main-content no-results not-found">

			<p>Sorry, nothing was found. Try again?</p>

		</section>

		<!-- SEARCH FORM -->
		<?php get_search_form(); ?>

	<?php endif; ?>

</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>