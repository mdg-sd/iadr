<?php $attrs = get_extended_thumbnail_attributes();

if ( has_post_thumbnail() && $attrs['style'] != 'hidden' ) { ?>

	<div class="entry-image <?=$attrs['class']?>">

		<a href="<?=get_the_permalink();?>"><?php the_post_thumbnail( $attrs['size'] ); ?></a>

		<?php if ( get_the_post_thumbnail_caption() ) { ?>
		<div class="wp-post-image-caption">
			<?php the_post_thumbnail_caption(); ?>
		</div>
		<?php } ?>

	</div>

<?php }
