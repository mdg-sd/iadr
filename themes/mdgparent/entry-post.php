<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>

	<!-- HEADER -->
	<header class="entry-header">

		<!-- TITLE -->
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

		<!-- META -->
		<section class="entry-meta">
			<span class="entry-author vcard"><?php the_author_posts_link(); ?></span>
			<span class="meta-sep">|</span>
			<time class="entry-date" pubdate><?php the_time( get_option( 'date_format' ) ); ?></time>
		</section>

	</header>

	<!-- IMAGE -->
	<?php get_template_part('content', 'entry-image'); ?>

	<!-- CONTENT -->
	<div class="entry-content-container">

		<!-- BODY -->
		<section class="entry-content">
			<div class="entry-content-inner">
				<?php the_content(); ?>
			</div>
		</section>

		<!-- BLOCKS -->
		<?php the_blocks(); ?>

	</div>

	<!-- PAGINATION -->
	<?php get_template_part( 'nav', 'entry-content' ); ?>

	<!-- FOOTER -->
	<footer class="entry-footer">

		<!-- CATEGORIES -->
		<?php if ( has_category() ) : ?>
			<span class="cat-links">Posted in <?php the_category( ', ' ); ?></span>
		<?php endif;?>

		<!-- TAGS -->
		<?php if ( has_tag() ) : ?>
			<?php if ( has_category() ) { ?> <span class="meta-sep">|</span> <?php } ?>
			<span class="tag-links"><?php the_tags(); ?></span>
		<?php endif; ?>

		<!-- COMMENT COUNT -->
		<?php if ( comments_open() ) : ?>
			<?php if ( has_category() || has_tag() ) { ?> <span class="meta-sep">|</span> <?php } ?>
			<span class="comments-link"><a href="<?php echo get_comments_link(); ?>"><?php comments_number(); ?></a></span>
		<?php endif; ?>

	</footer>

</article>

<?php if ( is_single() ) { ?>

	<!-- COMMENTS -->
	<?php if ( !post_password_required() ) comments_template( '', true ); ?>

	<!-- NEXT & PREVIOUS -->
	<?php get_template_part( 'nav', 'entry' ); ?>

<?php } ?>
