<?php

// THEME SUPPORT
// -------------

	add_action( 'after_setup_theme', 'mdgparent_add_theme_supports' );

	function mdgparent_add_theme_supports() {

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'post-thumbnails' );

	}

// CONTENT FILTERS
// ---------------

	add_filter( 'wp_title', 'mdgparent_filter_wp_title' );

	function mdgparent_filter_wp_title( $title ) {
		return $title . esc_attr( get_bloginfo( 'name' ) );
	}

	add_filter( 'the_title', 'mdgparent_title' );

	function mdgparent_title( $title ) {
		if ( $title == '' ) {
			return '&rarr;';
		} else {
			return $title;
		}
	}

	add_filter( 'get_comments_number', 'mdgparent_comments_number' );

	function mdgparent_comments_number( $count ) {
		if ( !is_admin() ) {
			global $id;

			$comments = get_comments( 'status=approve&post_id=' . $id );
			$comments_by_type = separate_comments( $comments );

			return count( $comments_by_type['comment'] );

		} else {

			return $count;

		}
	}

// COMMENTS
// --------

	// Probably won't be used, since we prefer Disqus or other plugins, but keep these just in case.

	add_action( 'comment_form_before', 'mdgparent_enqueue_comment_reply_script' );

	function mdgparent_enqueue_comment_reply_script() {
		if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
	}

	function mdgparent_pings( $comment ) {

		$GLOBALS['comment'] = $comment;

		?><li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li><?php

	}

// CSS & JS
// --------

	add_action( 'wp_enqueue_scripts', 'mdgparent_load_main_css' );

	function mdgparent_load_main_css() {

		$css_mod_time = filemtime(get_stylesheet_directory() . '/css/main.css');
		wp_enqueue_style ( 'main', get_stylesheet_directory_uri() . '/css/main.css', false, $css_mod_time );

	}

	add_action( 'wp_enqueue_scripts', 'mdgparent_load_main_js' );

	function mdgparent_load_main_js() {

		$js_mod_time = filemtime(get_stylesheet_directory() . '/js/main.js');
		wp_enqueue_script ( 'main', get_stylesheet_directory_uri() . '/js/main.js', ['jquery'], $js_mod_time, true );

	}

// BODY CLASSES
// ------------

	add_filter( 'body_class', 'mdgparent_body_classes' );

	function mdgparent_body_classes( $classes ) {

		// Adds a class of custom-background-image to sites with a custom background image.
		if ( get_background_image() ) {
			$classes[] = 'custom-background-image';
		}

		// Adds a class of group-blog to sites with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}

		// Adds a class of no-sidebar to sites without active sidebar.
		if ( ! is_active_sidebar( 'sidebar-1' ) ) {
			$classes[] = 'no-sidebar';
		}

		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}

// ALIASES
// -------

	function get_display_title( $source = null ) {

		if ( function_exists('mdgcore_get_display_title') ) {
			return mdgcore_get_display_title($source);
		} else {
			return get_the_title();
		}

	}

	function hide_title( $source = null ) {

		if ( function_exists('mdgcore_hide_title') ) {
			return mdgcore_hide_title($source);
		} else {
			return false;
		}

	}

	function get_subtitle( $source = null ) {

		if ( function_exists('mdgcore_get_subtitle') ) {
			return mdgcore_get_subtitle($source);
		} else {
			return null;
		}

	}


	function get_extended_thumbnail_attributes() {

		if ( function_exists('mdgcore_get_extended_thumbnail_attributes') ) {

			$attrs = mdgcore_get_extended_thumbnail_attributes();

		} else {

			$attrs = [
				'style' => null,
				'size'  => null,
				'class' => 'default',
			];

		}

		return $attrs;

	}

	function the_blocks( $id = null ) {

		if ( function_exists('mdgcore_render_blocks') ) {
			mdgcore_render_blocks( $id );
		}

	}
