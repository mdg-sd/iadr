<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>

<nav class="archive-nav">

	<div class="archive-nav-previous"><?php next_posts_link(sprintf( '%s older', '<span class="meta-nav">&larr;</span>' ) ) ?></div>
	
	<div class="archive-nav-next"><?php previous_posts_link(sprintf( 'newer %s', '<span class="meta-nav">&rarr;</span>' ) ) ?></div>

</nav>

<?php endif; ?>