<?php

if ( get_field('attachment_no_singular') ) {

	$url = wp_get_attachment_url( $post->ID );

	wp_redirect( $url );
	exit;

}

get_template_part('singular');
