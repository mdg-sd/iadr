<?php get_header(); ?>

<main class="main">

	<header class="main-header">

		<h1 class="main-title">404</h1>

	</header>

	<div class="main-content-container">

		<section class="main-content not-found">

			<p>Sorry, nothing was found. Try again?</p>

			<!-- SEARCH FORM -->
			<?php get_search_form(); ?>

		</section>

	</div>

</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
