<form method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

	<div class="searchform-container">

		<div class="searchform-input-container">
			<label class="screen-reader-text" for="s">Search for:</label>
			<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
		</div>

		<div class="searchform-button-container">
			<button type="submit" id="searchsubmit"><i class="fa fa-search"></i></button>
		</div>

	</div>

</form>