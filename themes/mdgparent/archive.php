<?php get_header();

$page_title = 'Archive';
$page_type  = '';
$page_meta  = '';

// DATE

if ( is_day() ) {

	$page_type  = 'Day';
	$page_title = get_the_time( get_option( 'date_format' ) );

} elseif ( is_month() ) {

	$page_type  = 'Month';
	$page_title = get_the_time( 'F Y' );

} elseif ( is_year() )  {

	$page_type  = 'Year';
	$page_title = get_the_time( 'Y' );

// CATEGORY

} elseif ( is_category() ) {

	$page_type  = 'Category';
	$page_title = single_cat_title('', false);

	if ( !empty(category_description()) ) {
		$page_meta = apply_filters( 'archive_meta', category_description() );
	}

// TAG

} elseif ( is_tag() ) {

	$page_type  = 'Tag';	
	$page_title = single_tag_title('', false);

// AUTHOR

} elseif ( is_author() ) {

	$page_type  = 'Author';
	$page_title = get_the_author_link();

	if ( get_the_author_meta( 'user_description' ) != '' ) {
		$page_meta = apply_filters( 'archive_meta', get_the_author_meta( 'user_description' ) );
	}

}

rewind_posts(); ?>

<main class="main">

	<header class="main-header">

		<h1 class="main-title">

			<?php if ( !empty($page_type) ) : ?>
			<span class="main-type"><?=$page_type?></span>
			<?php endif; ?>

			<?=$page_title?>
			
		</h1>

		<?php if ( !empty($page_meta) ) : ?>
		<section class="main-meta"><?=$page_meta?></section>
		<?php endif; ?>

	</header>

	<section class="archive">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<!-- ENTRY -->
		<?php get_template_part( 'entry' ); ?>

	<?php endwhile; endif; ?>
	</section>

	<!-- PAGINATION -->
	<?php get_template_part( 'nav', 'archive' ); ?>

</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>