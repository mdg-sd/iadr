<?php if ( $post->post_parent == '0' ) return; ?>

<nav class="attachment-parent-nav">

	<a href="<?php echo get_permalink( $post->post_parent ); ?>" title="Back to <?esc_html( get_the_title( $post->post_parent ), 1 )?>" rev="attachment">
		<span class="meta-nav"><i class="fa fa-level-up-alt"></i></span>
		<?php echo get_the_title( $post->post_parent ); ?>
	</a>

</nav>