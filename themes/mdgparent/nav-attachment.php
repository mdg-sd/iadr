<nav class="attachment-nav">

	<div class="attachment-nav-previous">
		<?php previous_image_link( 'thumb', '<i class="fa fa-chevron-left"></i>' ); ?>
	</div>
	
	<div class="attachment-nav-next">
		<?php next_image_link( 'thumb', '<i class="fa fa-chevron-right"></i>' ); ?>
	</div>

</nav>