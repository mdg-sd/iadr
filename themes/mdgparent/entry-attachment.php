<?php

$file_url  = wp_get_attachment_url( $post->ID );
$file_name = basename( $file_url );
$caption   = $post->post_excerpt;
$image     = false; // default

if ( wp_attachment_is_image( $post->ID ) ) {
	$image     = wp_get_attachment_image_src( $post->ID, 'large' );
	$image_url = $image[0];
	$image_alt = get_post_meta( $post->ID, '_wp_attachment_image_alt', true);
} ?>

<!-- PARENT -->
<?php if ( is_attachment() ) get_template_part( 'nav', 'attachment-parent' ); ?>

<!-- ENTRY -->
<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>

	<header class="entry-header">

		<!-- TITLE -->
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

	</header>

	<div class="entry-content-container">

		<!-- ATTACHMENT -->
		<section class="entry-content">
			<div class="entry-content-inner">

				<?php if ( $image ) { ?>

					<!-- IMAGE -->
					<a href="<?=esc_attr($file_url)?>" title="<?=esc_attr(get_the_title())?>" rel="attachment" target="_blank"><img src="<?=$image_url?>" class="attachment-large" alt="<?=esc_attr($image_alt)?>" /></a>

					<!-- CAPTION -->
					<?php if ( !empty($caption) ) { ?>
					<div class="wp-post-image-caption"><?=esc_html($caption)?></div>
					<?php } ?>

				<?php } ?>

				<!-- DESCRIPTION -->
				<?php the_content(); ?>

				<!-- DOWNLOAD -->
				<div class="attachment-download">
					<a class="button" href="<?=esc_attr($file_url)?>" title="<?=esc_attr(get_the_title())?>" rel="attachment" download="<?=esc_attr($file_name)?>">
						<i class="fa fa-download"></i>
						<?=esc_html($file_name)?>
					</a>
				</div>

			<div class="entry-content-inner">
		</section>

	</div>

</article>

<!-- NEXT & PREVIOUS -->
<?php if ( is_attachment() ) get_template_part( 'nav', 'attachment' ); ?>
